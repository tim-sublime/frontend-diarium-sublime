import { UserCheck, UserX, Home, Cloud, Users, Mail, MessageCircle, Clock, CheckSquare, Calendar, BookOpen, Bookmark,Film } from 'react-feather'

const isCheckin = JSON.parse(localStorage.getItem("isCheckin"))
const isCheckout = JSON.parse(localStorage.getItem("isCheckout"))
var presensiitems = []

var ITEMS = [
    {
        menutitle:"Home Page",
        menucontent:"",
        Items:[
            { path: `${process.env.PUBLIC_URL}/dashboard/homepage`, icon:Home, type: 'link', title: 'Dashboard' },
        ]
    },
    {
        menutitle:"Presensi",
        menucontent:"",
        Items: presensiitems
    },
]

if(!isCheckin && !isCheckout){
    presensiitems = [
        { path: `${process.env.PUBLIC_URL}/presensi/checkin`,icon:UserCheck, title: 'Check In', type: 'link' },
        { path: `${process.env.PUBLIC_URL}/checkin/history`,icon:Bookmark, title: 'History', type: 'link' }
    ]
    ITEMS[1].Items = presensiitems
} 
else {
    if(isCheckin && !isCheckout){
        presensiitems = [
            { path: `${process.env.PUBLIC_URL}/presensi/checkout`,icon:UserX, title: 'Check Out', type: 'link' },
            { path: `${process.env.PUBLIC_URL}/checkin/history`,icon:Bookmark, title: 'History', type: 'link' }
        ]
        ITEMS[1].Items = presensiitems
    }
    else if(isCheckin && isCheckout){
        presensiitems = [
            { path: `${process.env.PUBLIC_URL}/checkin/history`,icon:Bookmark, title: 'History', type: 'link' }
        ]
        ITEMS[1].Items = presensiitems

    }
    ITEMS.push(
        {
            menutitle:"Activity",
            menucontent:"",
            Items:[
                { path: `${process.env.PUBLIC_URL}/activity/todo`,icon:Clock, type: 'link', title: 'Activity' },
                { path: `${process.env.PUBLIC_URL}/activity/activity`,icon:CheckSquare, type: 'link', title: 'Agenda'},
                { path: `${process.env.PUBLIC_URL}/activity/calendar`, icon:Calendar, type: 'link', title: 'Calendar' },
            ]
        },        
        {
            menutitle:"User Menu",
            menucontent:"",
            Items:[
                { path: `${process.env.PUBLIC_URL}/users/userEdit`,icon:Users, title: 'Edit Profile', type: 'link' },
            ]
        },    
        {
            menutitle:"Applications",
            menucontent:"",
            Items:[
                {
                    title: 'Blog',icon: Film, type: 'sub', active: false, children: [
                        { path: `${process.env.PUBLIC_URL}/app/blog/blogTimeline`, title: 'Blog Timeline', type: 'link' },
                        { path: `${process.env.PUBLIC_URL}/app/blog/blogPost`, title: 'Add Post', type: 'link' },
                    ]
                },
                { path: `${process.env.PUBLIC_URL}/app/notadinas`,icon:Mail, title: 'Nota Dinas', type: 'link', badge: "badge badge-danger",badgetxt:"55" },
                { path: `${process.env.PUBLIC_URL}/app/chataja`,icon:MessageCircle, title: 'ChatAja', type: 'link', badge: "badge badge-danger",badgetxt:"100" },
                { path: `${process.env.PUBLIC_URL}/app/email`,icon:Mail, title: 'Telkom Umail', type: 'link', badge: "badge badge-danger",badgetxt:"55" },
                { path: `${process.env.PUBLIC_URL}/app/dropup`,icon:Cloud, title: 'Telkom Dropup', type: 'link' },
                { path: `${process.env.PUBLIC_URL}/app/learning/`,icon:BookOpen, title: 'Digital Learning', type: 'link' },
    
            ]
        }
    )
} 

export const MENUITEMS = ITEMS;