import React, { Fragment,useState,useEffect } from 'react';
import Breadcrumb from '../../../layout/breadcrumb'
import { Container, Row, Col, Card, CardHeader, CardBody, Media, Button, Form, FormGroup, Input, Label } from 'reactstrap'
import {Email,MarekjecnoMailId,BOD,DDMMYY,Designer,ContactUs,ContactUsNumber,LocationDetails,JOHANDIO,UserProfileDesc1,UserProfileDesc2,UserProfileDesc3,Comment,MarkJecno,Like,Follower,Following,Location} from '../../../constant'
import SweetAlert from 'sweetalert2'
import { fetchBlogs } from '../../../redux/actionCreators';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import {useForm} from 'react-hook-form'

const BlogTimeline = (props) => {

  const categories = [
    { name: 'All Post' },
    { name: 'Uncategorized' },
    { name: 'Fashion' },
    { name: 'Food' },
    { name: 'Travel' },
    { name: 'Music' },
    { name: 'Lifestyle' },
    { name: 'Fitness' },
    { name: 'DIY' },
    { name: 'Sports' },
    { name: 'Finance' },
    { name: 'Political' },
    { name: 'Parenting' },
    { name: 'Business' },
    { name: 'Personal' },
    { name: 'Movie' },
    { name: 'Automotive' },
    { name: 'News' },
    { name: 'Gaming' }
  ]

  const userInfo = JSON.parse(localStorage.getItem("userInfo"))
  const fullname = userInfo.firstname +' '+ userInfo.lastname;

  const dispatch = useDispatch()
  const [activeCategory,setActiveCategory] = useState("1")
  const [blogs, setBlogs] = useState([]);
  const [doingAct, setDoingAct] = useState([]);
  const [doneAct, setDoneAct] = useState([]);
  const { register, handleSubmit, errors } = useForm();

  const [url, setUrl] = useState();

  useEffect(() => {
    dispatch(fetchBlogs())
    .then(res => {setBlogs(res.payload);
      // setDoingAct(res.payload.filter(item => item.status === 'Doing'));
      // setDoneAct(res.payload.filter(item => item.status === 'Done'))
    })    
  },[])

  const filterPost = (data) => {    
    if (data !== '') {      
      setBlogs(blogs.filter(item => item.category === data.category));
    } else {
      errors.showMessages();
    }
  };

  const readUrl = (event) => {
    if (event.target.files.length === 0)
      return;
    var mimeType = event.target.files[0].type;

    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      setUrl(reader.result)
    }
  }
  return (
    <Fragment>
      <Breadcrumb parent="Blog" title="Blog Timeline" />
      <Container fluid={true}>
        <div className="user-profile">
          <Row>
            <Col sm="12">
              <Card className="card hovercard text-center">
                <CardHeader className="cardheader"></CardHeader>
                <div className="user-image">
                  <div className="avatar"><Media body alt="" src={require("../../../assets/images/avatar/"+userInfo.avatar)} data-intro="This is Profile image" /></div>
                </div>
                <div className="info">
                  <Row>
                    <Col sm="6" lg="4" className="order-sm-1 order-xl-0">
                      <Row >
                        <Col md="6">
                          <div className="ttl-info text-left">
                            <h6><i className="fa fa-envelope mr-2"></i> {Email}</h6><span>{userInfo.email}</span>
                          </div>
                        </Col>
                        <Col md="6">
                          <div className="ttl-info text-left ttl-sm-mb-0">
                          <h6><i className="fa fa-phone"></i>   {ContactUs}</h6><span>{userInfo.phone}</span>
                          </div>
                        </Col>
                      </Row>
                    </Col>
                    <Col sm="12" lg="4" className="order-sm-0 order-xl-1">
                      <div className="user-designation">
                        <div className="title"><a target="_blank" href="#javascript">{fullname}</a></div>
                        <div className="desc mt-2">{userInfo.jobPosition}</div>
                      </div>
                    </Col>
                    <Col sm="6" lg="4" className="order-sm-2 order-xl-2">
                      <Row>
                        <Col md="6">
                          <div className="ttl-info text-left ttl-xs-mt">
                          <h6><i className="fa fa-location-arrow"></i> {Location}</h6><span>{userInfo.address}</span>
                          </div>
                        </Col>
                        <Col md="6">
                          <div className="ttl-info text-left ttl-sm-mb-0">
                            <h6><i className="fa fa-location-arrow"></i> {"Job Location"}</h6><span>{"TelkomGroup"}</span>
                          </div>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  <hr />
                  <div className="social-media step4" data-intro="This is your Social details">
                    <ul className="list-inline">
                      <li className="list-inline-item"><a href="#javascript"><i className="fa fa-facebook"></i></a></li>
                      <li className="list-inline-item"><a href="#javascript"><i className="fa fa-google-plus"></i></a></li>
                      <li className="list-inline-item"><a href="#javascript"><i className="fa fa-twitter"></i></a></li>
                      <li className="list-inline-item"><a href="#javascript"><i className="fa fa-instagram"></i></a></li>
                      <li className="list-inline-item"><a href="#javascript"><i className="fa fa-rss"></i></a></li>
                    </ul>
                  </div>
                  <div className="follow">
                    <Row>
                      <Col col="6" className="text-md-right border-right">
                        <div className="follow-num counter">{"25869"}</div><span>{Follower}</span>
                      </Col>
                      <Col col="6" className="text-md-left">
                        <div className="follow-num counter">{"659887"}</div><span>{Following}</span>
                      </Col>
                    </Row>
                  </div>
                </div>
              </Card>
            </Col>    
            <Col sm="12" xl="12">
              <Card>           
                <CardBody>
                  <Form className="form theme-form" onSubmit={handleSubmit(filterPost)}>
                    <Row>     
                    <Label className="col-sm-1 col-form-label">{"Category"}</Label>
                    <Col sm="3">
                      <Input type="select" name="category" className="form-control digits" innerRef={register({ required: true })}>
                          {categories.map((item) => <option value={item.name}>{item.name}</option>)}
                      </Input>
                    </Col>
                    <Col sm="6">
                      <Input
                          className="form-control"
                          type="text"
                          placeholder="search by title"          
                        />  
                    </Col>
                    <Col>
                    <Button  color="primary" className=" btn-block mr-1">{"Filter"}</Button>
                    </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>

            {blogs.map((item, i) =>
            <Col sm="12" key={i}>
              <Card>
                <div className="profile-img-style">
                  <Row>
                    <Col sm="8">
                      <div className="media"><Media className="img-thumbnail rounded-circle mr-3" src={require("../../../assets/images/avatar/"+item.user.avatar)} alt="Generic placeholder image" />
                        <div className="media-body align-self-center">
                          <h5 className="mt-0 user-name">{item.user.firstname +' '+item.user.lastname }</h5>
                        </div>
                      </div>
                    </Col>
                    <Col sm="4" className="align-self-center">
                      <div className="float-sm-right"><small>{"Created at " + item.createdAt.slice(0, 10)}</small></div>
                    </Col>
                  </Row>
                  <hr />
                  <Row>
                    <Col lg="12" xl="4">
                      <div id="aniimated-thumbnials-3"><a href="#javascript"><Media body className="img-fluid rounded" src={require("../../../assets/images/blog/"+ item.thumbnail)} alt="gallery" /></a></div>
                      <div className="like-comment mt-4 like-comment-lg-mb">
                        <ul className="list-inline">
                          <li className="list-inline-item border-right pr-3">
                            <label className="m-0"><a href="#javascript"><i className="fa fa-th-list"></i></a> </label><span className="ml-2 counter">{item.category}</span>
                          </li>
                          <li className="list-inline-item border-right pr-3">
                            <label className="m-0"><a href="#javascript"><i className="fa fa-heart"></i></a>  {Like}</label><span className="ml-2 counter">{"2659"}</span>
                          </li>
                          <li className="list-inline-item ml-2">
                            <label className="m-0"><a href="#javascript"><i className="fa fa-comment"></i></a>  {Comment}</label><span className="ml-2 counter">{"569"}</span>
                          </li>
                        </ul>
                      </div>      
                    </Col>
                    <Col xl="6">
                      <h2>{item.title}</h2>
                      {/* <p>{UserProfileDesc3}</p>  */}
                      <p>{item.content.replace(/<[^>]+>/g, '').slice(0, 400)}</p>
                      <Link className="btn btn-success mr-2" style={{ color: 'white' }} to={`${process.env.PUBLIC_URL}/app/blog/blogSingle/${item._id}`}>{"Read More"}</Link>
                    </Col>
                  </Row>
                </div>
              </Card>
            </Col>
            )} 
          </Row>
        </div>
      </Container>
    </Fragment>
  );
}

export default BlogTimeline;