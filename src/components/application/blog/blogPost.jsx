import React, { Fragment, useState } from 'react';
import Breadcrumb from '../../../layout/breadcrumb';
import CKeditor from 'react-ckeditor-component';
import { Typeahead } from 'react-bootstrap-typeahead';
import Dropzone from 'react-dropzone-uploader'
import { Container, Row, Col, Card, CardHeader, CardBody, Form, FormGroup, Label, Input, Button } from "reactstrap"
import {PostEdit,Title,Type,Category,Content,Post,Discard,Text,Audio,Video,Image} from "../../../constant";
import {useForm} from 'react-hook-form'
import { useDispatch } from 'react-redux';
import { createBlog } from '../../../redux/actionCreators';
import {withRouter,Link} from 'react-router-dom'

const BlogPost = (props) => {

  const [content, setContent] = useState("");
  const dispatch = useDispatch()
  const { register, handleSubmit, errors } = useForm();

  const handleChange = (e, editor) => {
    const data = e.editor.getData()
    setContent(data)
  }

  const createNewPost = (data) => {    
    if (data !== '') {      
      data.content = content      
      dispatch(createBlog(data))
      .then(() => {window.location.href = `${process.env.PUBLIC_URL}/app/blog/blogTimeline`})
    } else {
      errors.showMessages();
    }
  };

  const categories = [
    { name: 'Uncategorized' },
    { name: 'Fashion' },
    { name: 'Food' },
    { name: 'Travel' },
    { name: 'Music' },
    { name: 'Lifestyle' },
    { name: 'Fitness' },
    { name: 'DIY' },
    { name: 'Sports' },
    { name: 'Finance' },
    { name: 'Political' },
    { name: 'Parenting' },
    { name: 'Business' },
    { name: 'Personal' },
    { name: 'Movie' },
    { name: 'Automotive' },
    { name: 'News' },
    { name: 'Gaming' }
  ]

  const getUploadParams = ({ meta }) => { return { url: 'https://httpbin.org/post' } }
  const handleChangeStatus = ({ meta, file }, status) => { }
  
  return (
    <Fragment>
      <Breadcrumb parent="Blog" title="Add Post" />
      <Container fluid={true}>
        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <h5>{"Add New Post"}</h5>
              </CardHeader>
              <CardBody className="add-post">
                <Form className="row needs-validation" onSubmit={handleSubmit(createNewPost)}>
                  <Col sm="12">
                    <FormGroup>
                      <Label for="title">{Title}:</Label>
                      <Input className="form-control" name="title" id="title" type="text" placeholder="Post Title" innerRef={register({ required: true })} />
                      <span style={{ color: "red" }}>{errors.title && 'Title is required'}</span>
                      <div className="valid-feedback">{"Looks good!"}</div>
                    </FormGroup>
                    <FormGroup>
                      <Label>{Type}:</Label>
                      <div className="m-checkbox-inline">
                        <Label for="Text">
                          <Input className="radio_animated" value="Text" type="radio" name="type" innerRef={register({ required: true })} />{Text}
                            </Label>
                        <Label for="Image">
                          <Input className="radio_animated" value="Image" type="radio" name="type" innerRef={register({ required: true })} />{Image}
                            </Label>
                        <Label for="Audio">
                          <Input className="radio_animated" value="Audio" type="radio" name="type" innerRef={register({ required: true })}/>{Audio}
                            </Label>
                        <Label for="Video">
                          <Input className="radio_animated" value="Video" type="radio" name="type" innerRef={register({ required: true })} />{Video}
                            </Label>
                      </div>
                    </FormGroup>
                    <FormGroup>
                      <Label>{"Category"}</Label>
                      <Input type="select" name="category" className="form-control digits" innerRef={register({ required: true })}>
                        {categories.map((item) => <option value={item.name}>{item.name}</option>)}
                      </Input>
                    </FormGroup>   
                    <div className="email-wrapper">
                      <div className="theme-form">
                        <FormGroup>
                          <Label>{Content}:</Label>
                          <CKeditor
                            // editor={ClassicEditor}
                            activeclassName="p10"
                            content={content} 
                            events={{
                              "change": handleChange
                            }}
                          />
                        </FormGroup>
                      </div>
                    </div>
                  </Col>                      
                  <div className="btn-showcase">
                    <FormGroup className="mt-4">
                      <Button color="primary" className="mr-3">{Post}</Button>
                      <Link to={`${process.env.PUBLIC_URL}/app/blog/blogTimeline`}>
                        <Button color="danger">{Discard}</Button>
                      </Link>
                    </FormGroup>
                  </div>                    
         
                  </Form>     
                      
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
}

export default BlogPost;