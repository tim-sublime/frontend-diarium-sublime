import React, { Fragment,useState,useEffect } from 'react';
import Breadcrumb from '../../../layout/breadcrumb'
import {Container,Row,Col,Card,CardHeader,CardBody} from 'reactstrap';
import { useDispatch } from 'react-redux';
import { fetchBlog } from '../../../redux/actionCreators';
import {withRouter,Link,useParams} from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';

const  BlogSingle = (props) => {

  const dispatch = useDispatch()
  const [post, setPost] = useState([]);
  let {postId}  = useParams();  
  // const fullname = post.user.firstname +' '+post.user.lastname

    useEffect(() => {
      dispatch(fetchBlog(postId)).then(res => {
        setPost(res.payload)   
      }).then(console.log(post))
    },[post])     

    return (
         <Fragment>
         <Breadcrumb parent="Blog" title="Blog Post"/>
          <Container fluid={true}>
            <Row>  
            {post.map((item, i) =>     
              <Col sm="12">
                <Card>
                  <CardHeader>
                    <h5>{item.title}</h5>
                    <div className="like-comment mt-4 like-comment-lg-mb">
                        <ul className="list-inline">
                        <li className="list-inline-item border-right pr-3">
                            <label className="m-0"><a href="#javascript"><i className="fa fa-user"></i></a> </label><span className="ml-2 counter">{item.user.firstname+' '+item.user.lastname}</span>
                          </li>
                          <li className="list-inline-item border-right pr-3">
                            <label className="m-0"><a href="#javascript"><i className="fa fa-th-list"></i></a> </label><span className="ml-2 counter">{item.category}</span>
                          </li>
                          <li className="list-inline-item border-right pr-3">
                            <label className="m-0"><a href="#javascript"><i className="fa fa-heart"></i></a>  {"Like"}</label><span className="ml-2 counter">{"2659"}</span>
                          </li>
                          <li className="list-inline-item ml-2">
                            <label className="m-0"><a href="#javascript"><i className="fa fa-comment"></i></a>  {"Comment"}</label><span className="ml-2 counter">{"569"}</span>
                          </li>
                        </ul>
                      </div>  
                  </CardHeader>
                  <CardBody>
                    {ReactHtmlParser(item.content)}
                  </CardBody>
                </Card>
              </Col>  
            )}  
            </Row>
          </Container>   
         </Fragment> 
    );
}

export default BlogSingle;