import React,{useState,useEffect, Fragment} from 'react';
import Breadcrumb from '../../../layout/breadcrumb'
import CKEditor from "react-ckeditor-component";
import {Container,Row,Col,Card,CardBody,Media,Label,Input,Button,Form,FormGroup,Table} from 'reactstrap'
import {useDispatch,useSelector} from 'react-redux'
import {UPDATE_EMAIL_TYPES, GET_ALL_EMAIL_ASYN,WATCH_ALL_TYPE_EMAIL, GET_ALL_TYPE_ASYN} from '../../../redux/actionTypes'
import {UPDATE_NODIN_TYPES, GET_ALL_NODIN_ASYN,WATCH_ALL_TYPE_NODIN, GET_ALL_NODINTYPE_ASYN} from '../../../redux/actionTypes'
import {Dropdown,DropdownItem,DropdownToggle,DropdownMenu} from 'reactstrap'
import user from '../../../assets/images/user/igor.jpg';
import email1 from '../../../assets/images/email/1.jpg';
import email2 from '../../../assets/images/email/2.jpg';
import email3 from '../../../assets/images/email/3.jpg';
import telkom from '../../../assets/images/logo/telkom.png';
// import {groupBy} from '../../../redux/email/action'
import {groupBy} from '../../../redux/notadinas/action'
import {IGOR,IGOREMAIL,NEWMAIL,Inbox,AllMail,Sent,Draft,Trash,IMPORTANT,Starred,UNREAD,Spam,OUTBOX,UPDATE,ALERT,NOTES,NoMailFound,NewMessage,To,ATTACHMENTS,DownloadAll,Reply,ReplyAll,Forward,Send,Messages,More,Subject,Earnings,NewUser,Products} from '../../../constant'
import { Archive,FileText,MessageCircle } from 'react-feather';
import CountUp from 'react-countup';
import { Typeahead } from 'react-bootstrap-typeahead';
import Dropzone from 'react-dropzone-uploader'

var images = require.context('../../../assets/images', true);

const Notadinas = (props) => {

  const dispatch = useDispatch();
  const usersList = useSelector(content => content.NodinApp.allNodins);
  const TypesOfData = useSelector(mailTypes => mailTypes.NodinApp.types);
  const [compose, setCompose] = useState(true);
  const [dropdownOpen, setOpen] = useState(false);
  const [type, setType] = useState('Incoming Letter');
  const [emailIds, setEmailIds] = useState([]);
  // eslint-disable-next-line
  const [mailData, setMailData] = useState([]);
  const [checked, setchecked] = useState(false);
  const [selectedFav, setSelectedFav] = useState(false);
  const [singleMailRecord, setSingleMailRecord] = useState({});

  const toggle = () => setOpen(!dropdownOpen);

  const karyawan = [
    { name: '' },
    { name: 'EVP TELKOM REGIONAL X' },
    { name: 'DEVP MARKETING TELKOM REGIONAL X' },
    { name: 'DEVP INFRASTRUCTURE TELKOM REGIONAL X' },
    { name: 'SM BUSINESS PLANNING & PERFORMANCE REG X' },
    { name: 'SM GENERAL AFFAIR REG X' },
    { name: 'SM HUMAN CAPITAL REG X' },
    { name: 'SM PAYMENT COLLECTION & FINANCE REG X' },
    { name: 'OSM CONSUMER MARKETING REG X' },
    { name: 'OSM CUSTOMER CARE REG X' },
    { name: 'OSM REG ENTERPRISE, GOVERNMENT & BUSINESS SERVICE REG X' },
    { name: 'OSM REGIONAL WHOLESALE SERVICE REG X' },
    { name: 'OSM ACCESS MANAGEMENT REG X' },
    { name: 'OSM REGIONAL NETWORK OPERATION REG X' },
    { name: 'OSM MANAGED SERVICE OPERATION REG X' },
    { name: 'OSM REGIONAL OPERATION CENTER REG X' },
    { name: 'OSM PLANNING, ENGINEERING & DEPLOYMENT REG X' },
  ]

  const klasifikasi = [
    { name: '' },
    { name: 'HK 000 HUKUM' },
    { name: 'IN 000 DAPENTEL' },
    { name: 'KA 000 DAPENTEL' },
    { name: 'KU 000 KEUANGAN' },
    { name: 'LB 000 PENELITIAN DAN PENGEMBANGAN' },
    { name: 'LG 000 LOGISTIK' },
    { name: 'LP 000 PENGELOLAAN DATA DAN LAPORAN' },
    { name: 'PD 000 PENDIDIKAN DAN PELATIHAN' },
    { name: 'PR 000 PUBLIC RELATION' },
    { name: 'PS 000 PERSONALIA' },
    { name: 'PW 000 PENGAWASAN' },
    { name: 'TK 000 TEKNIK TELEKOMUNIKASI' },
    { name: 'UM 000 UMUM' },
    { name: 'YN 000 PELAYANAN JASA/FASILITAS TELKO' },
  ]

  useEffect(() => {
      if(usersList !== null){
      const result = groupBy(usersList, (item) => {
        return [item.type];
      });
      dispatch({type : WATCH_ALL_TYPE_NODIN,result});
    }
    
   }, [dispatch,usersList]);

  const getUploadParams = ({ meta }) => { return { url: 'https://httpbin.org/post' } }
  const handleChangeStatus = ({ meta, file }, status) => { }

  
  const dynamicImage = (image) => {
    return images(`./${image}`);
  }
   
  const clickCompose = () => {
      setCompose(true);
  }

  const selectTypes = (types) => {
    setSelectedFav(false)
    setType(types)   
  }

  const selectFev = (types) => {
    setSelectedFav(true)
  }

  const moveEmails = (val) => {
    [...document.querySelectorAll('.checkbox_animated')].map((input) => {
        if (input.checked) {
            let fakeInput = {
                target: {
                    value: input.value,
                    checked: false
                }
            }
            input.checked = !input.checked;
            selectedmail(fakeInput);
        }
        return null;
    })
    for (var i = 0; i < usersList.length; i++) {
        if (emailIds.includes(usersList[i].id)) {
            usersList[i].type = val;
        }
    }

    var result = groupBy(usersList, function (item) {
        return [item.type];
    });

     dispatch({type : GET_ALL_NODIN_ASYN, usersList})
     dispatch({ type: GET_ALL_NODINTYPE_ASYN, result })

    }

  const selectedCompose = (email) => {
    setCompose(false);
    setSingleMailRecord(email);
   }

  const addFavourite = (singleMailRecord) => {
    dispatch({ type: UPDATE_NODIN_TYPES, payload: singleMailRecord })
  }

  const selectedmail = (e, emailID) => {
    const IDs = emailIds;
    setchecked(e.target.checked);
    if (emailIds == null) {
        setEmailIds(mailData)
    } else {
        if (e.target.checked) {
            IDs.push(emailID)
            setEmailIds(IDs)
            const arr = [...new Set(emailIds)];;
            setEmailIds(arr)
        } else {
            setEmailIds(mailData)
        }
    }
  }   
    
    return (
      <Fragment>
        <Breadcrumb parent="Apps" title="Notadinas TelkomGroup"/>
        <Container fluid={true}>
            <div className="email-wrap">
              <Row>
              <Col sm="6" xl="4" lg="6">
                <Card className="o-hidden">
                  <CardBody className="bg-success b-r-4 pb-2 card-body">
                    <div className="media static-top-widget">
                      <div className="align-self-center text-center"><Archive /></div>
                      <div className="media-body"><span className="m-0">{"Need Follow Up"}</span>
                        <h4 className="mb-0 counter"><CountUp end={3012} /></h4><Archive className="icon-bg" />
                      </div>
                    </div>
                      <a className="mt-3 btn-success btn-block btn-mail"  onClick={() => selectTypes('Need Follow Up')} href="#javascript">
                        <i className="icofont icofont-envelope mr-2"></i> 
                          {"Click for Detail"}
                      </a>
                  </CardBody>
                </Card>
              </Col>
              <Col sm="6" xl="4" lg="6">
                <Card className="o-hidden">
                  <CardBody className="bg-primary b-r-4 pb-2 card-body">
                    <div className="media static-top-widget">
                      <div className="align-self-center text-center"><FileText /></div>
                      <div className="media-body"><span className="m-0">{"Incoming Letter"}</span>
                        <h4 className="mb-0 counter"><CountUp end={6659} /></h4><FileText className="icon-bg" />
                      </div>
                    </div>
                      <a className="mt-3 btn-primary btn-block btn-mail"  onClick={() => selectTypes('Incoming Letter')} href="#javascript">
                        <i className="icofont icofont-envelope mr-2"></i> 
                          {"Click for Detail"}
                      </a>
                  </CardBody>
                </Card>
              </Col>
              <Col sm="6" xl="4" lg="6">
                <Card className="o-hidden">
                  <CardBody className="bg-secondary b-r-4 pb-2 card-body">
                    <div className="media static-top-widget">
                      <div className="align-self-center text-center"><MessageCircle /></div>
                      <div className="media-body"><span className="m-0">{"Disposition"}</span>
                        <h4 className="mb-0 counter"><CountUp end={7098} /></h4><MessageCircle className="icon-bg" />
                      </div>
                    </div>
                      <a className="mt-3 btn-secondary btn-block btn-mail"  onClick={() => selectTypes('Disposition')} href="#javascript">
                        <i className="icofont icofont-envelope mr-2"></i> 
                          {"Click for Detail"}
                      </a>
                  </CardBody>
                </Card>
              </Col>                          
                <Col xl="3" md="6 box-col-6">
                  <div className="email-right-aside">
                    <Card className="email-body">
                      <div className="pr-0 b-r-light">
                        <div className="email-top">
                            <Row>
                                <Col xl="12">
                                    <h5>{selectedFav ? 'Favourite' : type}</h5>
                                </Col>
                                <Col className="text-right">
                                    <div className="dropdown">
                                      <a className="btn btn-primary btn-block btn-sm"  onClick={clickCompose} href="#javascript">
                                        <i className="icofont icofont-envelope mr-2"></i> 
                                        {"COMPOSE"}
                                      </a>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                      <div className="inbox custom-scrollbar">
                      {/* <div className="inbox custom-scrollbar" style={{maxHeight: 1000, height: 950}}> */}
                        {
                          selectedFav ?
                              usersList.filter((email) => email.favourite === true).length > 0 ?
                                  usersList.filter((email) => email.favourite === true).map((list, index) => {
                                      return (
                                          <Media key={index} onClick={() => selectedCompose(list)}>
                                              <Label className="d-block">
                                                  <Input className="checkbox_animated" 
                                                      name="chk-ani" type="checkbox" onChange={(e) => selectedmail(e, list.id)} defaultChecked={checked} />
                                              </Label>
                                              <div className="media-size-email">
                                                  <Media body className="mr-3 rounded-circle img-50 " src={list.image ? dynamicImage(list.image) : ''} alt="" />
                                              </div>
                                              <Media body>
                                                  <h6>{list.name}  <small><span className="digits">({list.date})</span></small></h6>
                                                  <p>{list.cc},</p>
                                              </Media>
                                          </Media>
                                      )
                                  })
                                  :
                                  <div className="search-not-found text-center ng-star-inserted" >
                                      <div className="">
                                          <Media alt="" className="second-search" src={images(`./search-not-found.png`)} />
                                          <p className="mb-0">{NoMailFound}</p>
                                      </div>
                                  </div>
                              :
                              TypesOfData[type] ? TypesOfData[type].map((list, index) => {
                                  return (
                                      <Media key={index} onClick={() => selectedCompose(list)}>
                                          <Label className="d-block" htmlFor="chk-ani">
                                              <input className="checkbox_animated" id="chk-ani"
                                                  name="chk-ani" type="checkbox" onChange={(e) => selectedmail(e, list.id)} defaultChecked={checked} />
                                          </Label>
                                          <div className="media-size-email">
                                              <Media body className="mr-3 rounded-circle img-50" src={list.image ? dynamicImage(list.image) : ''} alt="" />
                                          </div>
                                          <Media body>
                                              <h6>{list.name}</h6>
                                              {/* <h6>{list.name}  <small><span className="digits">({list.date})</span></small></h6> */}
                                              <p>{list.cc},</p>
                                          </Media>
                                      </Media>
                                  )
                              }) 
                              :
                              type === 'AllEmails' ?
                                  usersList.map((list, index) => {
                                      return (
                                          <Media key={index} onClick={() => selectedCompose(list)}>
                                              <Label className="d-block" htmlFor="chk-ani">
                                                  <Input className="checkbox_animated"
                                                      name="chk-ani" type="checkbox" onChange={(e) => selectedmail(e, list.id)} defaultChecked={checked} />
                                              </Label>
                                              <div className="media-size-email">
                                                  <Media body className="mr-3 rounded-circle img-50" src={list.image ? dynamicImage(list.image) : ''} alt="" />
                                              </div>
                                              <Media body>
                                                  <h6>{list.name}  <small><span className="digits">({list.date})</span></small></h6>
                                                  <p>{list.cc},</p>
                                              </Media>
                                          </Media>
                                      )
                                  })
                                  :
                                  <div className="search-not-found text-center ng-star-inserted" >
                                      <div className="">
                                          <Media alt="" className="second-search" src={images(`./search-not-found.png`)} />
                                          <p className="mb-0">{NoMailFound}</p>
                                      </div>
                                  </div>
                          }
                        </div>
                      </div>
                    </Card>
                  </div>
                </Col>
                <Col xl="9" md="12 box-col-12">
                  <div className="email-right-aside">
                    <Card className="email-body radius-left">
                      <div className="pl-0">
                        <div className="tab-content">
                        <div className={`tab-pane fade ${compose ? 'active show' : ''}`} id="pills-darkhome" role="tabpanel" aria-labelledby="pills-darkhome-tab">
                            <div className="email-compose">
                              <div className="email-top compose-border">
                                <Row>
                                  <Col sm="8 xl-50">
                                    <h4 className="mb-0">{"New Letter"}</h4>
                                  </Col>
                                  <Col sm="4 xl-50" className="btn-middle">
                                    <Button color="primary" className="btn-block btn-mail text-center mb-0 mt-0">
                                      <i className="fa fa-paper-plane mr-2"></i>{Send}
                                    </Button>
                                  </Col>
                                </Row>
                              </div>
                              <div className="email-wrapper">
                                <Form className="theme-form">
                                  <FormGroup>
                                    <div className="col-form-Label">{"Kepada"}:
                                          <Typeahead
                                            id="multiple-typeahead"
                                            className="mt-2"
                                            clearButton
                                            // defaultSelected={data.slice(0, 5)}
                                            labelKey="name"
                                            multiple
                                            options={karyawan}
                                            // placeholder="Select Your Name...."
                                          />
                                    </div>
                                  </FormGroup>
                                  <FormGroup>
                                    <Label>{"Dari"}</Label>
                                    <Input type="select" name="select" className="form-control digits" defaultValue="dari">
                                      {karyawan.map((item) => <option value={item.name}>{item.name}</option>)}
                                    </Input>
                                  </FormGroup>
                                  <FormGroup>
                                    <div className="col-form-Label">{"Tembusan"}:
                                          <Typeahead
                                            id="multiple-typeahead"
                                            className="mt-2"
                                            clearButton
                                            // defaultSelected={data.slice(0, 5)}
                                            labelKey="name"
                                            multiple
                                            options={karyawan}
                                            // placeholder="Select Your Name...."
                                          />
                                    </div>
                                  </FormGroup>
                                  <FormGroup>
                                    <div className="col-form-Label">{"Pemeriksa"}:
                                          <Typeahead
                                            id="multiple-typeahead"
                                            className="mt-2"
                                            clearButton
                                            // defaultSelected={data.slice(0, 5)}
                                            labelKey="name"
                                            multiple
                                            options={karyawan}
                                            // placeholder="Select Your Name...."
                                          />
                                    </div>
                                  </FormGroup>
                                  <FormGroup>
                                    <Label>{"Klasifikasi Masalah"}</Label>
                                    <Input type="select" name="select" className="form-control digits">
                                      {klasifikasi.map((item) => <option value={item.name}>{item.name}</option>)}
                                    </Input>
                                  </FormGroup>                                  
                                  <FormGroup>
                                    <Label >{"Perihal"}</Label>
                                    <Input className="form-control" type="text"/>
                                  </FormGroup>
                                  <FormGroup>
                                    <Label className="text-muted">{Messages}</Label>
                                    <CKEditor
                                        activeclassName="p10"
                                    />                                                          
                                  </FormGroup>                                  
                                </Form>
                                <Form className="m-b-20">
                                  <div className="m-0 dz-message needsclick">
                                    <Dropzone
                                      getUploadParams={getUploadParams}
                                      onChangeStatus={handleChangeStatus}
                                      maxFiles={1}
                                      multiple={false}
                                      canCancel={false}
                                      inputContent="Drop files here or click to upload."
                                      styles={{
                                        dropzone: { width: '100%', height: 50 },
                                        dropzoneActive: { borderColor: 'green' },
                                      }}
                                    />
                                  </div>
                                </Form>
                              </div>
                            </div>
                          </div>
                          <div className={`tab-pane fade ${compose !== true ? 'active show' : ''}`}>
                              <div className="email-content">
                                  <div className="email-top">
                                      <Row>
                                          <Col md="6 xl-100" sm="12">
                                              <Media>
                                                  <Media className="mr-3 rounded-circle img-50" src={singleMailRecord.image ? dynamicImage(singleMailRecord.image) : ''} alt="" />
                                                  <Media body>
                                                      <h6>{singleMailRecord.name} <small><span className="digits">{singleMailRecord.date}</span> <span className="digits">6:00</span> AM</small></h6>
                                                      <p>{singleMailRecord.cc}</p>
                                                  </Media>
                                              </Media>
                                          </Col>
                                          <Col md="6" sm="12">
                                              <div className="float-right d-flex" onClick={() => addFavourite(singleMailRecord)}>
                                                  <p className="user-emailid">{"Lormlpsa"}<span className="digits">{"23"}</span>{"@company.com"}</p>
                                                  <i className={`fa fa-star-o f-18 mt-1 ${singleMailRecord.favourite ? 'starred' : ''} `} ></i>
                                              </div>
                                          </Col>
                                      </Row>
                                  </div>
                                  <div className="email-wrapper">
                                      <img className="img-fluid pull-right" src={telkom} alt="" />
                                      <div style={{marginTop: 60}}><h5>{"Nota Dinas"}</h5></div>
                                      <Table borderless className="m-b-20">                                        
                                        <tbody>
                                            <tr>                                                
                                                <td className="p-0">{"Nomor"}</td>
                                                <td className="p-0">{": C.Tel.22/UM 000/TCU-04040000/2020"}</td>
                                            </tr>
                                            <tr>                                                
                                                <td className="p-0">{"Kepada"}</td>
                                                <td className="p-0">{": Sdr-sdr Terlampir"}</td>
                           
                                            </tr>
                                            <tr>                                               
                                                <td className="p-0">{"Dari"}</td>
                                                <td className="p-0">{": CEO AMA CORPU"}</td>
                                    
                                            </tr>
                                        </tbody>
                                      </Table>
                                      <div className="text-justify pr-5">
                                        <ol className="d-block">
                                          <li className="pb-3">{"Menunjuk:"}
                                            <ol className="d-block" type="a">
                                              <li className="pb-2">{"PR. Dir HCM Nomor: 206.01/r.00/PD000/COP-B0010000/2012 perihal pengelolaan Coaching, Mentoring & Teaching;"}</li>
                                              <li className="pb-2">{"PR. Dir HCM Nomor: 204.03/r.02/HK200/COP-J2000000/2015 perihal sistem rekrut;"}</li>
                                              <li className="pb-2">{"Nota Dinas SM DSPA Nomor: C.Tel.277/UM 000/TCU-12000000/2020 tanggal 16 September 2020 perihal permohonan bantuan mentor Telkom Athon;"}</li>
                                              <li className="pb-2">{"Nota Dinas SM DSPA Nomor: C.Tel.300/UM 000/TCU-12000000/2020 tanggal 28 September 2020 perihal Undangan Opening Telkom Athon 2020;"}</li>
                                              <li className="pb-2">{"Nota Dinas SM DSPA Nomor: C.Tel.376/UM 000/TCU-12000000/2020 tanggal 19 November 2020 perihal Undangan sebagai Peserta pada Pelaksanaan Webinar/Sharing Session Telkom Athon (UI/UX)."}</li>
                                              <li className="pb-2">{"Nota Dinas SM DSPA Nomor: C.Tel.378/UM 000/TCU-12000000/2020 tanggal 24 November 2020 perihal Undangan sebagai Peserta pada Pelaksanaan Webinar/Sharing Session Telkom Athon (Data Science)."}</li>
                                              <li className="pb-0">{"Nota Dinas CEO AMA CORPU Nomor: C.Tel.14/UM 000/TCU-04040000/2020 tanggal 29 November 2020 perihal Undangan Technical Meeting Telkom Athon Competition 2020."}</li>
                                            </ol>
                                          </li>
                                          <li className="pb-3">{"Menimbang:"}
                                            <ol className="d-block" type="a">
                                              <li className="pb-2">{"Program Telkom Athon merupakan project based learning yang mewadahi karyawan Telkom potensial untuk meningkatkan kapabilitas digital. Telkom Athon mendukung dan memfasilitasi pengembangan karyawan untuk mengupgrade digital skill dan potensi yang dimiliki sehingga dapat memberikan impact yang signifikan untuk pengembangan digital talent bagi  perusahaan."}</li>
                                              <li className="pb-2">{"Salah satu rangkaian acara Telkom Athon adalah Telkom Athon Competition. Acara ini bertujuan untuk melakukan mapping digital talent ready bagi seluruh peserta stream yang terdiri dari data science, cyber security, programmer dan UI/UX."}</li>
                                              <li className="pb-2">{"Aktivitas Telkom Athon Competition dimulai dari technical meeting, pengerjaan use case, penjurian 1, penjurian 2 dan rewarding."}</li>
                                              <li className="pb-0">{"Berdasarkan point (c), saat ini kompetisi memasuki tahap penjurian 1 dengan jenis kegiatan yakni presentasi hasil pengerjaan use case."}</li>
                                            </ol>
                                          </li>
                                          <li className="pb-3">{"Sesuai hal di atas, kami ingin mengundang Saudara yang terkait dalam agenda presentasi hasil yang akan dilaksanakan pada :"}
                                          <Table borderless>                                        
                                        <tbody>
                                            <tr>                                                
                                                <td className="p-0">{"Hari/Tanggal"}</td>
                                                <td className="p-0"><strong>{": Rabu/ 23 Desember 2020"}</strong></td>
                                            </tr>
                                            <tr>                                                
                                                <td className="p-0">{"Waktu"}</td>
                                                <td className="p-0"><strong>{": 08.00 - 12.00 WIB"}</strong></td>
                           
                                            </tr>
                                            <tr>                                               
                                                <td className="p-0">{"Tempat"}</td>
                                                <td className="p-0"><strong>{": Online via Zoom  http://ubah.in/presentasi1programmer"}</strong></td>
                                    
                                            </tr>
                                        </tbody>
                                      </Table>
                                          </li>
                                          <li className="pb-3">{"Apabila terdapat pertanyaan dan informasi lebih lanjut, team Saudara dapat menghubungi PIC kami yaitu Sdr. Aditya Darma P  (M: +62 821-3075-7132)."}</li>
                                          <li>{"Demikian informasi yang dapat kami sampaikan, atas perhatian dan kerjasama Saudara kami ucapkan terima kasih."}
                                          <br/>
                                          <strong>{"Salam Telkom Group : WIN DIGITAL!!"}</strong>
                                          </li>
                                        </ol>
                                      </div>        
                                      <div style={{marginTop: 80,marginBottom: 80}}><p className="txt-dark">{"Bandung, 2 Januari 2021"}</p></div>
                                      <p className="txt-dark"><strong>{"Prasetiyo Raharjo, Mti"}</strong></p>
                                      <p className="txt-dark">{"NIK: 720307"}</p>                  
              
                                      <hr />
                                      <div className="d-inline-block">
                                          <h6 className="text-muted"><i className="icofont icofont-clip"></i> {ATTACHMENTS}</h6><a className="text-muted text-right right-download" href="#javascript"><i className="fa fa-long-arrow-down mr-2"></i>{DownloadAll}</a>
                                          <div className="clearfix"></div>
                                      </div>
                                      <div className="attachment">
                                          <ul className="list-inline">
                                              <li className="list-inline-item"><img className="img-fluid" src={email1} alt="" /></li>
                                              <li className="list-inline-item"><img className="img-fluid" src={email2} alt="" /></li>
                                              <li className="list-inline-item"><img className="img-fluid" src={email3} alt="" /></li>
                                          </ul>
                                      </div>
                                      <hr />
                                      <div className="action-wrapper">
                                          <ul className="actions">
                                              <li><a className="text-muted" href="#javascript"><i className="fa fa-reply mr-2"></i>{Reply}</a></li>
                                              <li><a className="text-muted" href="#javascript"><i className="fa fa-reply-all mr-2"></i>{ReplyAll}</a></li>
                                              <li><a className="text-muted" href="#javascript"><i className="fa fa-share mr-2"></i></a>{Forward}</li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                      </div>
                    </Card>
                  </div>
                </Col>
              </Row>
            </div>
          </Container>
          </Fragment>
    );
}
export default Notadinas;