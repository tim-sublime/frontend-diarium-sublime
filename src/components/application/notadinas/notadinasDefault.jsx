import React, { Fragment,useEffect} from 'react';
import {useDispatch } from 'react-redux'
import Notadinas from './notadinas'
import {fetchAllEmail} from '../../../redux/email/action'
import {fetchAllNodin} from '../../../redux/notadinas/action'

const NotadinasDefault = (props) =>  {
    
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(fetchAllEmail());
        dispatch(fetchAllNodin());
    },[dispatch]);

    return (
        <Fragment>
            <Notadinas/>
        </Fragment>
    );
}

export default NotadinasDefault;