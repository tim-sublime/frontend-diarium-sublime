import React, { Fragment } from 'react';
import Breadcrumb from '../../../layout/breadcrumb'
import { Container, Row, Col, Card, CardHeader, CardBody, CardFooter, Button, Media, Form, Label, Input } from 'reactstrap'
import { DefaultStyle,COD,Fast,NoBorder,OfferStyleBorder,InlineStyle,Free,Submit,Cancel,VerticalStyle,HorizontalStyle,SolidBorderStyle,Local,XYZSeller,ABCSeller,Standard,DeliveryOption,BuyingOption } from "../../../constant";
const MegaOption = (props) => {
  return (
    <Fragment>
      <Breadcrumb parent="Form" title="Check-in" />
      <Container fluid={true}>
        <Row>          
          <Col sm="12" xl="6 xl-100 box-col-12">
            <div className="card height-equal">
              <CardBody>
                <Form className="mega-vertical">
                  <Row>
                    <Col sm="12">
                      <p className="mega-title m-b-5">{"How do you feel today?"}</p>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-danger mr-3">
                            <Input id="radio23" type="radio" name="radio1" value="option1" />
                            <Label for="radio23"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-danger digits">{"SICK"}</span></h6>                            
                            <img className="img-fluid" src={require("../../../assets/images/diarium/sakit.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-secondary mr-3">
                            <Input id="radio24" type="radio" name="radio1" value="option1" />
                            <Label for="radio24"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-secondary digits">{"NOT VERY WELL"}</span></h6>                            
                            <img className="img-fluid" src={require("../../../assets/images/diarium/kurangfit.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-success mr-3">
                            <Input id="radio25" type="radio" name="radio1" value="option1" />
                            <Label for="radio25"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-success digits">{"VERY WELL"}</span></h6>                            
                            <img className="img-fluid" src={require("../../../assets/images/diarium/sehat.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                    <Col sm="12">
                      <p className="mega-title m-b-5">{"Where do yo work today?"}</p>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-warning mr-3">
                            <Input id="radio26" type="radio" name="radio2" value="option1" />
                            <Label for="radio26"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-warning digits">{"OFFICE"}</span></h6>                            
                            <img className="img-fluid" src={require("../../../assets/images/diarium/wfo.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-info mr-3">
                            <Input id="radio27" type="radio" name="radio2" value="option1" />
                            <Label for="radio27"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-info digits">{"HOMEY"}</span></h6>                            
                            <img className="img-fluid" src={require("../../../assets/images/diarium/wfh.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-primary mr-3">
                            <Input id="radio28" type="radio" name="radio2" value="option1" />
                            <Label for="radio28"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-primary digits">{"SATELLITE"}</span></h6>                            
                            <img className="img-fluid" src={require("../../../assets/images/diarium/wfso.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              <CardFooter className=" text-right">
                <Button color="primary" className="m-r-15" type="submit">{Submit}</Button>
                <Button color="light" type="submit">{Cancel}</Button>
              </CardFooter>
            </div>
          </Col>          
        </Row>
      </Container>
    </Fragment>
  );
}

export default MegaOption;