import React, { Fragment,useState } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import { Container, Row, Col, Card, CardBody, CardFooter, Button, Media, Form, Label, Input } from 'reactstrap'
import { Submit,Cancel } from "../../constant";
import { useDispatch } from 'react-redux';
import { userCheckin } from '../../redux/actionCreators';
import {useForm} from 'react-hook-form'

const Checkin = (props) => {

  const dispatch = useDispatch()
  const { register, handleSubmit, errors } = useForm();
  const [checkinWaktu, setCheckinWaktu] = useState(new Date())

  const checkin = (data) => {
    if (data !== '') {
      data.checkin_waktu = checkinWaktu.toISOString()
      console.log(data)
      dispatch(userCheckin(data))
      // props.history.push(`${process.env.PUBLIC_URL}/activity/activity`)
    } else {
      errors.showMessages();
    }
  };

  return (
    <Fragment>
      <Breadcrumb parent="Presensi" title="Check-in" />
      <Container fluid={true}>
        <Row>          
          <Col sm="12" xl="6 xl-100 box-col-12">
            <div className="card height-equal">
            <Form className="mega-vertical" onSubmit={handleSubmit(checkin)}>
              <CardBody>                
                  <Row>
                    <Col sm="12">
                      <p className="mega-title m-b-5">{"How do you feel today?"}</p>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-danger mr-3">
                            <Input id="kondisi1" type="radio" name="checkin_kondisi" value="sakit" innerRef={register({ required: true })}/>
                            <Label for="kondisi1"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-danger digits">{"SICK"}</span></h6>                            
                            <img className="img-fluid" src={require("../../assets/images/diarium/sakit.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-secondary mr-3">
                            <Input id="kondisi2" type="radio" name="checkin_kondisi" value="kurangfit" innerRef={register({ required: true })}/>
                            <Label for="kondisi2"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-secondary digits">{"NOT VERY WELL"}</span></h6>                            
                            <img className="img-fluid" src={require("../../assets/images/diarium/kurangfit.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-success mr-3">
                            <Input id="kondisi3" type="radio" name="checkin_kondisi" value="sehat" innerRef={register({ required: true })}/>
                            <Label for="kondisi3"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-success digits">{"VERY WELL"}</span></h6>                            
                            <img className="img-fluid" src={require("../../assets/images/diarium/sehat.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                    <Col sm="12">
                      <p className="mega-title m-b-5">{"Where do yo work today?"}</p>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-warning mr-3">
                            <Input id="lokasi1" type="radio" name="checkin_lokasi" value="wfo" innerRef={register({ required: true })}/>
                            <Label for="lokasi1"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-warning digits">{"OFFICE"}</span></h6>                            
                            <img className="img-fluid" src={require("../../assets/images/diarium/wfo.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-info mr-3">
                            <Input id="lokasi2" type="radio" name="checkin_lokasi" value="wfh" innerRef={register({ required: true })}/>
                            <Label for="lokasi2"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-info digits">{"HOMEY"}</span></h6>                            
                            <img className="img-fluid" src={require("../../assets/images/diarium/wfh.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                    <Col sm="4">
                      <Card>
                        <Media className="p-20">
                          <div className="radio radio-primary mr-3">
                            <Input id="lokasi3" type="radio" name="checkin_lokasi" value="wfso" innerRef={register({ required: true })}/>
                            <Label for="lokasi3"></Label>
                          </div>
                          <Media body>
                            <h6 className="mt-0 mega-title-badge"> <span className="badge badge-primary digits">{"SATELLITE"}</span></h6>                            
                            <img className="img-fluid" src={require("../../assets/images/diarium/wfso.png")} alt=""/>
                          </Media>
                        </Media>
                      </Card>
                    </Col>
                  </Row>
                
              </CardBody>
              <CardFooter className=" text-right">
                <Button color="primary" className="m-r-15" type="submit">{Submit}</Button>
                <Button color="light" type="submit">{Cancel}</Button>
              </CardFooter>
              </Form>
            </div>
          </Col>          
        </Row>
      </Container>
    </Fragment>
  );
}

export default Checkin;