import React, { Fragment,useState,useEffect } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import {Container,Row,Col,Card,CardHeader,Table} from "reactstrap";
import { TableHeadOptions } from "../../constant";
import { useDispatch } from 'react-redux';
import { fetchPresensi,fetchCheckin } from '../../redux/actionCreators';

const History = () => {

    const dispatch = useDispatch()
    const [presensis, setPresensis] = useState([]);
    useEffect(() => {
        dispatch(fetchPresensi()).then(res => setPresensis(res.payload))
      },[])
    
    return (
        <Fragment>
            <Breadcrumb parent="Presensi" title="History"/>
            <Container fluid={true}>
                <Row>
                    <Col sm="12">
                        <Card>
                            <CardHeader>
                                <h5>{"History Check-in & Check-out"}</h5>                                
                            </CardHeader>
                            <div className="card-block row">
                                <Col sm="12" lg="12" xl="12">
                                    <div className="table-responsive">
                                        <Table>
                                            <thead className="thead-dark">
                                                <tr>
                                                    <th scope="col">{"#"}</th>
                                                    <th scope="col">{"Tanggal"}</th>
                                                    <th scope="col">{"Waktu Checkin"}</th>
                                                    <th scope="col">{"Kondisi Checkin"}</th>
                                                    <th scope="col">{"Lokasi Checkin"}</th>
                                                    <th scope="col">{"Waktu Checkout"}</th>
                                                    <th scope="col">{"Kondisi Checkout"}</th>
                                                    <th scope="col">{"Lokasi Checkout"}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {presensis.map((item, i) =>
                                                <tr key={i}>
                                                    <th scope="row">{i+1}</th>
                                                    <td>{String(item.checkin_waktu).slice(0,10)}</td>
                                                    <td>{String(item.checkin_waktu).slice(11,19)}</td>
                                                    <td>{item.checkin_kondisi}</td>
                                                    <td>{item.checkin_lokasi}</td>
                                                    <td>{String(item.checkout_waktu).slice(11,19)}</td>
                                                    <td>{item.checkout_kondisi}</td>
                                                    <td>{item.checkout_lokasi}</td>
                                                    
                                                </tr>
                                            )}
                                            </tbody>
                                        </Table>
                                    </div>
                                </Col>
                            </div>
                        </Card>
                    </Col>                        
                </Row>
            </Container>
        </Fragment>
            );
        };
        
export default History;