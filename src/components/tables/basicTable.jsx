import React, { Fragment } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import {Container,Row,Col,Card,CardHeader,Table} from "reactstrap";
import { BasicTable,InverseTable,InverseTablePrimaryBackground,HoverableRows,ContextualClasses,TextBackgroundUtilities,TableHeadOptions,StripedRow,StripedRowInverseTable,Caption,ResponsiveTables,BreckpointSpecific } from "../../constant";

const BasicTables = () => {
    return (
        <Fragment>
            <Breadcrumb parent="Check-in" title="History"/>
            <Container fluid={true}>
                <Row>
                    <Col sm="12">
                        <Card>
                            <CardHeader>
                                <h5>{TableHeadOptions}</h5>
                                <span>{"Similar to tables and dark tables, use the modifier classes"} <code>{".thead-dark"}</code>  {"to make"} <code>{"thead"}</code> {"appear light or dark gray."}</span>
                            </CardHeader>
                            <div className="card-block row">
                                <Col sm="12" lg="12" xl="12">
                                    <div className="table-responsive">
                                        <Table>
                                            <thead className="thead-dark">
                                                <tr>
                                                    <th scope="col">{"#"}</th>
                                                    <th scope="col">{"First Name"}</th>
                                                    <th scope="col">{"Last Name"}</th>
                                                    <th scope="col">{"Username"}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">{"1"}</th>
                                                    <td>{"Mark"}</td>
                                                    <td>{"Otto"}</td>
                                                    <td>{"@mdo"}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">{"2"}</th>
                                                    <td>{"Jacob"}</td>
                                                    <td>{"Thornton"}</td>
                                                    <td>{"@fat"}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">{"3"}</th>
                                                    <td>{"Larry"}</td>
                                                    <td>{"the Bird"}</td>
                                                    <td>{"@twitter"}</td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                    </div>
                                </Col>
                            </div>
                        </Card>
                    </Col>                        
                </Row>
            </Container>
        </Fragment>
            );
        };
        
export default BasicTables;