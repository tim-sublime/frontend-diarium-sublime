import React, { Fragment,useState } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
// import Dropzone from 'react-dropzone-uploader'
import {Container,Row,Col,Card,CardBody,Form,FormGroup,Label,Input,Button} from 'reactstrap'
import DatePicker from "react-datepicker";
import {useForm} from 'react-hook-form'
import {addNewProject} from '../../redux/project-app/action'
import { useDispatch } from 'react-redux';
import {withRouter,Link} from 'react-router-dom'
import {ProjectStatus,ProgressLevel,StartingDate,EndingDate,EnterSomeDetails,Add,Cancel,Done,Doing} from '../../constant'
import { createActivity } from '../../redux/actionCreators';

const AddActivity = (props) => {

    const dispatch = useDispatch()
    const { register, handleSubmit, errors } = useForm();
    const [startDate, setstartDate] = useState(new Date())
    const [endDate, setendDate] = useState(startDate)

    const handleStartDate = date => {
      setstartDate(date);
      setendDate(date)
    };

    const handleEndDate = date => {
      setendDate(date);
    };
    
    // const getUploadParams = ({ meta }) => { 
    //     return { 
    //       url: 'https://httpbin.org/post' 
    //     }
    // }
    
    // called every time a file's `status` changes
    // const handleChangeStatus = ({ meta, file }, status) => { }

    const AddProject = (data) => {
      if (data !== '') {
        dispatch(addNewProject(data))
        props.history.push(`${process.env.PUBLIC_URL}/activity/activity`)
      } else {
        errors.showMessages();
      }
    };

    const createNewActivity = (data) => {
      if (data !== '') {
        data.startDate = startDate.toISOString()
        data.endDate = endDate.toISOString()
        console.log(data)
        dispatch(createActivity(data))
        props.history.push(`${process.env.PUBLIC_URL}/activity/activity`)
      } else {
        errors.showMessages();
      }
    };

    return (
        <Fragment>
        <Breadcrumb parent="Activity" title="Add New Agenda" /> 
        <Container fluid={true}>
            <Row>
              <Col sm="12">
                <Card>
                  <CardBody>
                    <Form className="theme-form" onSubmit={handleSubmit(createNewActivity)}>
                      <Row>
                        <Col>
                          <FormGroup>
                            <Label>{"Agenda Title"}</Label>
                            <Input className="form-control" type="text"  name="title" placeholder="Agenda Title *" innerRef={register({ required: true })} />
                            <span style={{ color: "red" }}>{errors.title && 'Title is required'}</span>
                          </FormGroup>
                        </Col>
                      </Row>                      
                      <Row>
                        <Col sm="4">
                        <FormGroup>
                            <Label>{"Priority Rate"}</Label>
                            <Input type="select" className="form-control digits" name="priority_rate"  innerRef={register({ required: true })}>
                              <option>{"Low"}</option>
                              <option>{"Medium"}</option>
                              <option>{"High"}</option>
                            </Input>
                          </FormGroup>
                        </Col>
                        <Col sm="4">
                          <FormGroup>
                            <Label>{ProgressLevel}</Label>
                            <Input type="number"  name="progress_level" className="form-control digits" innerRef={register({ required: true })}>
                            </Input>
                          </FormGroup>
                        </Col>
                        <Col sm="4">
                          <FormGroup>
                            <Label>{"Agenda Status"}</Label>
                            <Input type="select" name="status" placeholder="Select Status" className="form-control digits" innerRef={register({ required: true })}>
                              <option value="Done">{Done}</option>
                              <option value="Doing">{Doing}</option>
                            </Input>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>                        
                        <Col sm="4">
                          <FormGroup>
                            <Label>{StartingDate}</Label>
                            <DatePicker className="datepicker-here form-control"  selected={startDate} onChange={handleStartDate} />
                          </FormGroup>
                        </Col>
                        <Col sm="4">
                          <FormGroup>
                            <Label>{EndingDate}</Label>
                            <DatePicker className="datepicker-here form-control"  selected={endDate} endDate={endDate} onChange={handleEndDate} minDate={startDate} />
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <FormGroup>
                            <Label>{EnterSomeDetails}</Label>
                            <Input  type="textarea" className="form-control" name="description" rows="3" innerRef={register({ required: true })}/>
                            <span style={{ color: "red" }}>{errors.description && 'Some Details is required'}</span>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <FormGroup className="mb-0">
                              <Button color="success" className="mr-3">{Add}</Button>
                              <Link to={`${process.env.PUBLIC_URL}/activity/activity`}>
                              <Button color="danger">{Cancel}</Button>
                              </Link>
                          </FormGroup>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </Fragment>
    );
}

export default withRouter(AddActivity);