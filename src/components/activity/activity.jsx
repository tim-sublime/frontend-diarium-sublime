import React, { Fragment,useState,useEffect } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import { Container, Row, Col, Card, CardBody, FormGroup, Progress, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap'
import { Target, Info, CheckCircle, PlusCircle } from 'react-feather';
import {Link} from 'react-router-dom';
import { useDispatch,useSelector } from 'react-redux';
import {Done,All,Doing} from '../../constant'
import SweetAlert from 'sweetalert2'
import { fetchActivities, deleteActivity } from '../../redux/actionCreators';

const Activity = (props) => {
  
  const dispatch = useDispatch()
  const [activeTab,setActiveTab] = useState("1")
  const allProject = useSelector(content => content.Projectapp.all_Project);
  const doingProject = useSelector(content => content.Projectapp.doing_Project);
  const doneProject = useSelector(content => content.Projectapp.done_Project);
  const [activities, setActivities] = useState([]);
  const [doingAct, setDoingAct] = useState([]);
  const [doneAct, setDoneAct] = useState([]);

  useEffect(() => {
    dispatch(fetchActivities())
    .then(res => {setActivities(res.payload);
      setDoingAct(res.payload.filter(item => item.status === 'Doing'));
      setDoneAct(res.payload.filter(item => item.status === 'Done'))})    
  },[activities])

  const removeActivity = (id) => {
    SweetAlert.fire({
      title: 'Are you sure?',
      text: "Once deleted, you will not be able to recover this agenda!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ok',
      cancelButtonText: 'cancel',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        dispatch(deleteActivity(id))
        SweetAlert.fire(
          'Deleted!',
          'Your agenda has been deleted.',
          'success'
        )
      }
      else {
        SweetAlert.fire(
          'Your activity is safe!'
        )
      }
    })
  }

  return (
    <Fragment>
      <Breadcrumb parent="Activity" title="Agenda" />
      <Container fluid={true}>
        <Row>
          <Col md="12" className="project-list">
            <Card>
              <Row>
                <Col sm="6">
                  <Nav tabs className="border-tab">
                    <NavItem><NavLink className={activeTab === "1" ? "active" : ''} onClick={() => setActiveTab("1")}><Target />{All}</NavLink></NavItem>
                    <NavItem><NavLink className={activeTab === "2" ? "active" : ''} onClick={() => setActiveTab("2")}><Info />{Doing}</NavLink></NavItem>
                    <NavItem><NavLink className={activeTab === "3" ? "active" : ''} onClick={() => setActiveTab("3")}><CheckCircle />{Done}</NavLink></NavItem>
                  </Nav>
                </Col>
                <Col sm="6">
                  <div className="text-right">
                    <FormGroup className="mb-0 mr-0"></FormGroup>
                    <Link className="btn btn-primary" style={{ color: 'white' }} to={`${process.env.PUBLIC_URL}/activity/add-activity`}> <PlusCircle />{"Create New Agenda"}</Link>
                  </div>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col sm="12">
            <Card>
              <CardBody>
                <TabContent activeTab={activeTab}>
                  <TabPane tabId="1">
                    <Row>
                      {activities.map((item, i) =>
                        <Col sm="12" className="mt-4" key={i}>
                          <div className="project-box">
                            <span className={`badge ${item.status === "Done" ? 'badge-success' : 'badge-primary'}`}>{item.status}</span>
                            <h6>{item.title}</h6>
                            <div className="media">
                              {/* <img className="img-20 mr-1 rounded-circle" src={require(`../../assets/images/${item.img}`)} alt="" /> */}
                              <div className="media-body">
                                <p>{"Created at " + item.createdAt.slice(0, 10)}</p>
                              </div>
                            </div>
                            <p>{item.description}</p>
                            <Row className="details">
                              <Col xs="6"><span>{"Priority Rate"} </span></Col>
                              <Col xs="6" className={item.progress_level === "Done" ? 'text-success' : 'text-primary'}>{item.priority_rate}</Col>
                              <Col xs="6"> <span>{"Start Date"}</span></Col>
                              <Col xs="6" className={item.badge === "Done" ? 'text-success' : 'text-primary'}>{String(item.startDate).slice(0,10)}</Col>
                              <Col xs="6"> <span>{"End Date"}</span></Col>
                              <Col xs="6" className={item.badge === "Done" ? 'text-success' : 'text-primary'}>{String(item.endDate).slice(0,10)}</Col>
                            </Row>                            
                            <div className="project-status mt-4">
                              <div className="media mb-0">
                                <p>{item.progress_level}% </p>
                                <div className="media-body text-right"><span>{Done}</span></div>
                              </div>
                              {item.progress_level === "100" ?
                                <Progress className="sm-progress-bar" color="success" value={item.progress_level} style={{ height: "5px" }} />
                                :
                                <Progress className="sm-progress-bar" striped color="primary" value={item.progress_level} style={{ height: "5px" }} />
                              }

                            </div>
                            <div className="action mt-4">
                              <Link className="btn btn-success mr-2" style={{ color: 'white' }} to={`${process.env.PUBLIC_URL}/activity/edit-activity/${item._id}`}>{"Edit Agenda"}</Link>
                              <a className="btn btn-danger" href="#javascript" onClick={() => removeActivity(item._id)}>{"Delete"}</a>
                            </div>
                          </div>
                        </Col>
                      )}
                    </Row>
                  </TabPane>
                  <TabPane tabId="2">
                  <Row>
                      {doingAct.map((item, i) =>
                        <Col sm="12" className="mt-4" key={i}>
                          <div className="project-box">
                            <span className={`badge ${item.status === "Done" ? 'badge-success' : 'badge-primary'}`}>{item.status}</span>
                            <h6>{item.title}</h6>
                            <div className="media">
                              {/* <img className="img-20 mr-1 rounded-circle" src={require(`../../assets/images/${item.img}`)} alt="" /> */}
                              <div className="media-body">
                                <p>{"Created at " + item.createdAt.slice(0, 10)}</p>
                              </div>
                            </div>
                            <p>{item.description}</p>
                            <Row className="details">
                              <Col xs="6"><span>{"Priority Rate"} </span></Col>
                              <Col xs="6" className={item.progress_level === "Done" ? 'text-success' : 'text-primary'}>{item.priority_rate}</Col>
                              <Col xs="6"> <span>{"Start Date"}</span></Col>
                              <Col xs="6" className={item.badge === "Done" ? 'text-success' : 'text-primary'}>{String(item.startDate).slice(0,10)}</Col>
                              <Col xs="6"> <span>{"End Date"}</span></Col>
                              <Col xs="6" className={item.badge === "Done" ? 'text-success' : 'text-primary'}>{String(item.endDate).slice(0,10)}</Col>
                            </Row>                            
                            <div className="project-status mt-4">
                              <div className="media mb-0">
                                <p>{item.progress_level}% </p>
                                <div className="media-body text-right"><span>{Done}</span></div>
                              </div>
                              {item.progress_level === "100" ?
                                <Progress className="sm-progress-bar" color="success" value={item.progress_level} style={{ height: "5px" }} />
                                :
                                <Progress className="sm-progress-bar" striped color="primary" value={item.progress_level} style={{ height: "5px" }} />
                              }

                            </div>
                            <div className="action mt-4">
                              <Link className="btn btn-success mr-2" style={{ color: 'white' }} to={`${process.env.PUBLIC_URL}/activity/edit-activity/${item._id}`}>{"Edit Agenda"}</Link>
                              <a className="btn btn-danger" href="#javascript" onClick={() => removeActivity(item._id)}>{"Delete"}</a>
                            </div>
                          </div>
                        </Col>
                      )}
                    </Row>
                  </TabPane>
                  <TabPane tabId="3">
                  <Row>
                      {doneAct.map((item, i) =>
                        <Col sm="12" className="mt-4" key={i}>
                          <div className="project-box">
                            <span className={`badge ${item.status === "Done" ? 'badge-success' : 'badge-primary'}`}>{item.status}</span>
                            <h6>{item.title}</h6>
                            <div className="media">
                              {/* <img className="img-20 mr-1 rounded-circle" src={require(`../../assets/images/${item.img}`)} alt="" /> */}
                              <div className="media-body">
                                <p>{"Created at " + item.createdAt.slice(0, 10)}</p>
                              </div>
                            </div>
                            <p>{item.description}</p>
                            <Row className="details">
                              <Col xs="6"><span>{"Priority Rate"} </span></Col>
                              <Col xs="6" className={item.progress_level === "Done" ? 'text-success' : 'text-primary'}>{item.priority_rate}</Col>
                              <Col xs="6"> <span>{"Start Date"}</span></Col>
                              <Col xs="6" className={item.badge === "Done" ? 'text-success' : 'text-primary'}>{String(item.startDate).slice(0,10)}</Col>
                              <Col xs="6"> <span>{"End Date"}</span></Col>
                              <Col xs="6" className={item.badge === "Done" ? 'text-success' : 'text-primary'}>{String(item.endDate).slice(0,10)}</Col>
                            </Row>                            
                            <div className="project-status mt-4">
                              <div className="media mb-0">
                                <p>{item.progress_level}% </p>
                                <div className="media-body text-right"><span>{Done}</span></div>
                              </div>
                              {item.progress_level === "100" ?
                                <Progress className="sm-progress-bar" color="success" value={item.progress_level} style={{ height: "5px" }} />
                                :
                                <Progress className="sm-progress-bar" striped color="primary" value={item.progress_level} style={{ height: "5px" }} />
                              }

                            </div>
                            <div className="action mt-4">
                              <Link className="btn btn-success mr-2" style={{ color: 'white' }} to={`${process.env.PUBLIC_URL}/activity/edit-activity/${item._id}`}>{"Edit Agenda"}</Link>
                              <a className="btn btn-danger" href="#javascript" onClick={() => removeActivity(item._id)}>{"Delete"}</a>
                            </div>
                          </div>
                        </Col>
                      )}
                    </Row>
                  </TabPane>
                </TabContent>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
}

export default Activity;