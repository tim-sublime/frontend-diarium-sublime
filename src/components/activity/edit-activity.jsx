import React, { Fragment,useState,useEffect } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
// import Dropzone from 'react-dropzone-uploader'
import {Container,Row,Col,Card,CardBody,Form,FormGroup,Label,Input,Button} from 'reactstrap'
import DatePicker from "react-datepicker";
import {useForm} from 'react-hook-form'
import {addNewProject} from '../../redux/project-app/action'
import { useDispatch,useSelector } from 'react-redux';
import {withRouter,Link,useParams} from 'react-router-dom'
import {ProjectStatus,ProgressLevel,StartingDate,EndingDate,EnterSomeDetails,Add,Cancel,Done,Doing} from '../../constant'
import { fetchActivity,updateActivity } from '../../redux/actionCreators';

const EditActivity = (props) => {

    const dispatch = useDispatch()
    const { register, handleSubmit, errors } = useForm();
    const [startDate, setstartDate] = useState(new Date());
    const [endDate, setendDate] = useState(new Date());
    const [activity, setActivity] = useState([]);
    const [status, setStatus] = useState('');
    const [priorityRate, setPriorityRate] = useState('');
    let {activityId}  = useParams();

    useEffect(() => {
      dispatch(fetchActivity(activityId)).then(res => {
        setActivity(res.payload.activity);
        setStatus(res.payload.activity.status);
        setPriorityRate(res.payload.activity.priority_rate);
      })
    },[]) 
        
    const handleStartDate = date => {
      setstartDate(date);
      setendDate(date)
    };

    const handleEndDate = date => {
      setendDate(date);
    };
    
    const getUploadParams = ({ meta }) => { 
        return { 
          url: 'https://httpbin.org/post' 
        }
    }
    

    // called every time a file's `status` changes
    const handleChangeStatus = ({ meta, file }, status) => { }

    const AddProject = data => {
      if (data !== '') {
        dispatch(addNewProject(data))
        props.history.push(`${process.env.PUBLIC_URL}/app/project/project-list`)
      } else {
        errors.showMessages();
      }
    };

    const updateNewActivity = (data) => {
      if (data !== '') {
        data.startDate = startDate.toISOString()
        data.endDate = endDate.toISOString()
        console.log(data)
        dispatch(updateActivity(activityId,data))
        props.history.push(`${process.env.PUBLIC_URL}/activity/activity`)
      } else {
        errors.showMessages();
      }
    };

    return (
        <Fragment>
        <Breadcrumb parent="Activity" title="Update Activity" /> 
        <Container fluid={true}>
            <Row>
              <Col sm="12">
                <Card>
                  <CardBody>
                    <Form className="theme-form" onSubmit={handleSubmit(updateNewActivity)}>
                      <Row>
                        <Col>
                          <FormGroup>
                            <Label>{"Activity Title"}</Label>
                            <Input className="form-control" type="text"  name="title" defaultValue={activity.title} innerRef={register({ required: true })} />
                            <span style={{ color: "red" }}>{errors.title && 'Title is required'}</span>
                          </FormGroup>
                        </Col>
                      </Row>                      
                      <Row>
                        <Col sm="4">
                        <FormGroup>
                            <Label>{"Priority Rate"}</Label>
                            <Input type="select" className="form-control digits" name="priority_rate" value={priorityRate} onChange={e => setPriorityRate(e.target.value)} innerRef={register({ required: true })}>
                              <option value="Low">{"Low"}</option>
                              <option value="Medium">{"Medium"}</option>
                              <option value="High">{"High"}</option>
                            </Input>
                          </FormGroup>
                        </Col>
                        <Col sm="4">
                          <FormGroup>
                            <Label>{ProgressLevel}</Label>
                            <Input type="number"  name="progress_level" className="form-control digits" defaultValue={activity.progress_level} innerRef={register({ required: true })}>
                            </Input>
                          </FormGroup>
                        </Col>
                        <Col sm="4">
                          <FormGroup>
                            <Label>{ProjectStatus}</Label>
                            <Input type="select" name="status" placeholder="Select Status" className="form-control digits" value={status} onChange={e => setStatus(e.target.value)} innerRef={register({ required: true })}>
                              <option value="Done">{Done}</option>
                              <option value="Doing">{Doing}</option>
                            </Input>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>                        
                        <Col sm="4">
                          <FormGroup>
                            <Label>{StartingDate}</Label>
                            <DatePicker className="datepicker-here form-control"  selected={startDate} onChange={handleStartDate} />
                          </FormGroup>
                        </Col>
                        <Col sm="4">
                          <FormGroup>
                            <Label>{EndingDate}</Label>
                            <DatePicker className="datepicker-here form-control"  selected={endDate} endDate={endDate} onChange={handleEndDate} minDate={startDate}/>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <FormGroup>
                            <Label>{EnterSomeDetails}</Label>
                            <Input  type="textarea" className="form-control" name="description" rows="3" defaultValue={activity.description} innerRef={register({ required: true })}/>
                            <span style={{ color: "red" }}>{errors.description && 'Some Details is required'}</span>
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <FormGroup className="mb-0">
                              <Button color="success" className="mr-3">{Add}</Button>
                              <Link to={`${process.env.PUBLIC_URL}/activity/activity`}>
                              <Button color="danger">{Cancel}</Button>
                              </Link>
                          </FormGroup>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </Fragment>
    );
}

export default withRouter(EditActivity);