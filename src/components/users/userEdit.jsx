import React, { Fragment,useState,useEffect } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import { Container, Row, Col, Card, CardHeader, CardBody, CardFooter, Media, Form, FormGroup, Label, Input } from 'reactstrap'
// import axios from 'axios'
import { Nik,MyProfile,MarkJecno,Designer,Password,Save,EditProfile,AboutMe,UpdateProfile,FirstName,LastName,Address,EmailAddress,Phone} from '../../constant'
import { useDispatch } from 'react-redux';
import { updateUser } from '../../redux/actionCreators';
import {useForm} from 'react-hook-form'

const UserEdit = (props) => {

  const dispatch = useDispatch()
  const { register, handleSubmit, errors } = useForm();
  const userInfo = JSON.parse(localStorage.getItem("userInfo"))
  const fullname = userInfo.firstname +' '+ userInfo.lastname

  // const [data,setData] = useState([])

  // useEffect(() => {
  //     axios.get(`${process.env.PUBLIC_URL}/api/user-edit-table.json`).then(res => setData(res.data))
  // },[])
  const updateNewUser = (data) => {
    if (data !== '') {
      console.log(data)
      dispatch(updateUser(userInfo._id,data))
      // props.history.push(`${process.env.PUBLIC_URL}/users/userEdit`)
    } else {
      errors.showMessages();
    }
  };

  return (
    <Fragment>
      <Breadcrumb parent="Users" title="Edit Profile" />
      <Container fluid={true}>
        <div className="edit-profile">
          <Row>
            <Col xl="4">
              <Card>
                <CardHeader>
                  <h4 className="card-title mb-0">{MyProfile}</h4>
                  <div className="card-options">
                    <a className="card-options-collapse" href="#javascript">
                      <i className="fe fe-chevron-up"></i>
                    </a>
                    <a className="card-options-remove" href="#javascript">
                      <i className="fe fe-x"></i>
                    </a>
                  </div>
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row className="mb-2 justify-content-center">
                      <div className="col-auto">
                        <Media className="rounded-circle" alt="" src={require("../../assets/images/avatar/"+userInfo.avatar)} />
                      </div>
                    </Row>
                    <Row className="mb-2 justify-content-center">
                      <Col className="text-center">
                        <h3 className="mb-1">{fullname}</h3>
                        <p className="mb-4">{userInfo.jobPosition}</p>
                      </Col>
                    </Row>
                    <FormGroup>
                      <Label className="form-label">{Nik}</Label>
                      <Input className="form-control" placeholder={userInfo.username} disabled />
                    </FormGroup>
                    <FormGroup>
                      <Label className="form-label">{Password}</Label>
                      <Input className="form-control" type="password"/>
                    </FormGroup>
                    <div className="form-footer">
                      <button className="btn btn-primary btn-block">{Save}</button>
                    </div>
                  </Form>
                </CardBody>
              </Card>
            </Col>
            <Col xl="8">
              <Form className="card" onSubmit={handleSubmit(updateNewUser)}>
                <CardHeader>
                  <h4 className="card-title mb-0">{EditProfile}</h4>
                  <div className="card-options">
                    <a className="card-options-collapse" href="#javascript">
                      <i className="fe fe-chevron-up"></i>
                    </a>
                    <a className="card-options-remove" href="#javascript">
                      <i className="fe fe-x"></i>
                    </a>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>                                 
                  <Col sm="6" md="6">
                      <FormGroup>
                        <Label className="form-label">{FirstName}</Label>
                        <Input className="form-control" type="text" name="firstname" defaultValue={userInfo.firstname} innerRef={register({ required: true })}/>
                      </FormGroup>
                    </Col>
                    <Col sm="6" md="6">
                      <FormGroup>
                        <Label className="form-label">{LastName}</Label>
                        <Input className="form-control" type="text" name="lastname" defaultValue={userInfo.lastname} innerRef={register({ required: true })}/>
                      </FormGroup>
                    </Col>
                    <Col md="12">
                      <FormGroup>
                        <Label className="form-label">Job Position</Label>
                        <Input className="form-control" type="text" name="jobPosition" defaultValue={userInfo.jobPosition} innerRef={register({ required: true })}/>
                      </FormGroup>
                    </Col>                    
                    <Col sm="6" md="6">
                      <FormGroup>
                        <Label className="form-label">{EmailAddress}</Label>
                        <Input className="form-control" type="email" name="email" defaultValue={userInfo.email} innerRef={register({ required: true })}/>
                      </FormGroup>
                    </Col>
                    <Col sm="6" md="6">
                      <FormGroup>
                        <Label className="form-label">{Phone}</Label>
                        <Input className="form-control" type="text" name="phone" defaultValue={userInfo.phone} innerRef={register({ required: true })}/>
                      </FormGroup>
                    </Col>                                 
                    <Col md="12">
                      <FormGroup>
                        <Label className="form-label">{Address}</Label>
                        <Input className="form-control" type="text" name="address" defaultValue={userInfo.address} innerRef={register({ required: true })}/>
                      </FormGroup>
                    </Col>
                    <Col md="12">
                      <div className="form-group mb-0">
                        <Label className="form-label">{AboutMe}</Label>
                        <Input type="textarea" className="form-control" name="aboutMe" rows="5" defaultValue={userInfo.aboutMe} innerRef={register({ required: true })}/>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter className="text-right">
                  <button className="btn btn-primary" type="submit">{UpdateProfile}</button>
                </CardFooter>
              </Form>
            </Col>
          </Row>
        </div>
      </Container>
    </Fragment>
  );
}

export default UserEdit;