// dashboard
import Homepage from '../components/dashboard/homepage'

import Checkin from '../components/presensi/checkin'
import Checkout from '../components/presensi/checkout'
import History from '../components/presensi/history'

import Activity from '../components/activity/activity'
import AddActivity from '../components/activity/add-activity'
import EditActivity from '../components/activity/edit-activity'
import Todo from '../components/activity/todo'
import DraggableCalendar from '../components/activity/draggableCalendar'

import UserEdit from "../components/users/userEdit"

import Notadinas from "../components/application/notadinas/notadinasDefault"
import BlogTimeline from "../components/application/blog/blogTimeline"
import BlogSingle from "../components/application/blog/blogSingle"
import BlogPost from "../components/application/blog/blogPost"
import ChatAja from "../components/application/chataja"
import Email from "../components/application/email/emailDefault"
import Dropup from '../components/application/dropup'
import Learninglist from "../components/application/learning/learning-list"

export const routes = [     
        { path:"/dashboard/homepage", Component:Homepage},

        { path:"/presensi/checkin", Component:Checkin},
        { path:"/presensi/checkout", Component:Checkout},
        { path:"/checkin/history", Component:History},

        { path:"/activity/activity", Component:Activity},
        { path:"/activity/add-activity", Component:AddActivity},
        { path:"/activity/edit-activity", Component:EditActivity},
        { path:"/activity/edit-activity/:activityId", Component:EditActivity},
        { path:"/activity/todo", Component:Todo},
        { path:"/activity/calendar", Component:DraggableCalendar},

        { path:"/users/userEdit", Component:UserEdit},

        { path:"/app/blog/blogTimeline", Component:BlogTimeline},                   
        { path:"/app/blog/blogPost", Component:BlogPost},
        { path:"/app/blog/blogSingle/:postId", Component:BlogSingle},
        { path:"/app/notadinas", Component:Notadinas},  
        { path:"/app/chataja", Component:ChatAja},
        { path:"/app/email", Component:Email},  
        { path:"/app/dropup", Component:Dropup},    
        { path:"/app/learning/", Component:Learninglist},
]