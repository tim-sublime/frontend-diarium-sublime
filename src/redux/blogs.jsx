import * as ActionTypes from './actionTypes';

export const Blogs = (state = {}, action) => {
    switch (action.type) {
      case ActionTypes.BLOG_CREATE_REQUEST:
        return { isLoading: true }
      case ActionTypes.BLOG_CREATE_SUCCESS:
        return { isLoading: false, blog: action.payload }
      case ActionTypes.BLOG_CREATE_FAIL:
        return { isLoading: false, error: action.payload }
      case ActionTypes.BLOG_UPDATE_REQUEST:
        return { isLoading: true }
      case ActionTypes.BLOG_UPDATE_SUCCESS:
        return { isLoading: false, blog: action.payload }
      case ActionTypes.BLOG_UPDATE_FAIL:
        return { isLoading: false, error: action.payload }
      case ActionTypes.BLOG_DELETE_REQUEST:
        return { loading: true }
      case ActionTypes.BLOG_DELETE_SUCCESS:
        return { isLoading: false, success: true }
      case ActionTypes.BLOG_DELETE_FAIL:
        return { isLoading: false, error: action.payload }
      default:
        return state
    }
  }