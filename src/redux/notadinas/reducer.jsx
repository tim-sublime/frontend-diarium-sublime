import {
    GET_NODINS,
    GET_ALL_NODINS,
    GET_NODIN_TYPES
} from '../actionTypes';

const initial_state = {
    allNodins: null,
    types: [],
    loading: false
};

export default (state = initial_state, action) => {
    
    switch (action.type) {

        case GET_NODINS:
            return { ...state };

        case GET_ALL_NODINS:
            return { ...state, allNodins: action.payload };

        case GET_NODIN_TYPES:
            const getTypes = action.payload;
            return { ...state, loading: true, types: getTypes };

        default: return { ...state };
    }
}
