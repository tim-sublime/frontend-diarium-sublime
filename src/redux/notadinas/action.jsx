import {
    GET_NODIN_TYPES,WATCH_NODIN,GET_ALL_NODINS
} from '../actionTypes';

export const fetchAllNodin = () => ({
    type:WATCH_NODIN
})

export const getAllNodins = (data) => ({
    type : GET_ALL_NODINS,
    payload : data
})

export const getAllTypes = (result) => ({
    type : GET_NODIN_TYPES,
    payload : result
})

export const groupBy = ( array , f ) => {
    var groups = {};
    array.forEach( function( o )
    {
        var group = f(o)[0]
        groups[group] = groups[group] || [];
        groups[group].push( o );  
    });
    return groups;
}

