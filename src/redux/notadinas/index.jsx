import {call,put,takeLatest} from "redux-saga/effects";
import {fetchNodinApi} from '../../api'
import {WATCH_NODIN,WATCH_ALL_TYPE_NODIN,GET_ALL_NODIN_ASYN,GET_ALL_NODINTYPE_ASYN} from '../../redux/actionTypes'
import {getAllNodins,getAllTypes} from '../../redux/notadinas/action'


function* fetchNodinAsyn() {
    const nodinData = yield call(fetchNodinApi);
    yield put(getAllNodins(nodinData.data));
}

function* fetchgetAllTypesAsyn({result}) {
    yield put(getAllTypes(result)); 
}

function* getAllNodinsAsyn({usersList}) {
    yield put(getAllNodins(usersList));
}

function* getAllTypesAsyn({result}) {
    yield put(getAllTypes(result));
}


export function* WatcherNodinApp() {
    yield takeLatest(WATCH_NODIN,fetchNodinAsyn)
    yield takeLatest(WATCH_ALL_TYPE_NODIN,fetchgetAllTypesAsyn)
    yield takeLatest(GET_ALL_NODIN_ASYN,getAllNodinsAsyn)
    yield takeLatest(GET_ALL_NODINTYPE_ASYN,getAllTypesAsyn)
}

