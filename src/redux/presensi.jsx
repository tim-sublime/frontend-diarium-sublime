import * as ActionTypes from './actionTypes';

export const Presensi = (state = {
  isCheckin: false,
  isCheckout: false
}, action) => {
    switch (action.type) {
      case ActionTypes.USER_CHECKIN_REQUEST:
        return { isLoading: true }
      case ActionTypes.USER_CHECKIN_SUCCESS:
        return { isLoading: false, checkin: action.payload, isCheckin: true  }
      case ActionTypes.USER_CHECKIN_FAIL:
        return { isLoading: false, error: action.payload }
      case ActionTypes.USER_CHECKOUT_REQUEST:
        return { isLoading: true }
      case ActionTypes.USER_CHECKOUT_SUCCESS:
        return { isLoading: false, checkout: action.payload, isCheckout: true}
      case ActionTypes.USER_CHECKOUT_FAIL:
        return { isLoading: false, error: action.payload }
      case ActionTypes.PRESENSI_FETCH_REQUEST:
        return { loading: true }
      case ActionTypes.PRESENSI_FETCH_SUCCESS:
        return { isLoading: false, presensi: action.payload }
      case ActionTypes.PRESENSI_FETCH_FAIL:
        return { isLoading: false, error: action.payload }
      default:
        return state
    }
  }