import {combineReducers} from 'redux'
import Todoapp from './todo/reducer'
import Ecommerce from './ecommerce/product/reducer'
import Filters from './ecommerce/filter/reducer'
import Wishlist from './ecommerce/wishlist/reducer'
import Cart from './ecommerce/cart/reducer'
import ChatApp from './chap-app/reducer'
import EmailApp from './email/reducer'
import NodinApp from './notadinas/reducer'
import Customizer from './customizer/reducer'
import Bookmarkapp from './bookmark/reducer'
import Taskapp from './task-app/reducer'
import Projectapp from './project-app/reducer'
import { Dishes } from './dishes';
import { Comments } from './comments';
import { Promotions } from './promotions';
import { Leaders } from './leaders';
import { favorites } from './favorites';
import { Auth, userRegisterReducer } from './auth';
import { Activities } from './activities';
import { Presensi } from './presensi';
import { Blogs } from './blogs';

const reducers = combineReducers({
    Todoapp,
    // data:Ecommerce,
    // filters:Filters,
    // Wishlistdata:Wishlist,
    // Cartdata:Cart,
    ChatApp,
    EmailApp,
    NodinApp,
    Customizer,
    // Bookmarkapp,
    // Taskapp,
    Projectapp,
    // dishes: Dishes,
    // comments: Comments,
    // promotions: Promotions,
    // leaders: Leaders,
    auth: Auth,
    // favorites,
    userRegister: userRegisterReducer,
    Presensi,
    Activities,
    Blogs
    })

export default reducers;