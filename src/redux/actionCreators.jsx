import * as ActionTypes from './actionTypes';
import { baseUrl } from '../shared/baseUrl';

export const register = (user) => async (dispatch) => {
    try {
      dispatch({
        type: ActionTypes.USER_REGISTER_REQUEST,
      })
  
      return fetch(baseUrl + 'users/signup', {
        method: "POST",
        body: JSON.stringify(user),
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })  
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(response => {
        if (response.success) {
            dispatch({
                type: ActionTypes.USER_REGISTER_SUCCESS,
                payload: response
                  
              })
            alert('You are successfuly registered!');
            window.location.href = `${process.env.PUBLIC_URL}/login`
        }
        else {
            var error = new Error('Error ' + response.status);
            error.response = response;
            throw error;
        }
    })
    .catch(error =>  { console.log('Register', error.message); alert('Your registration is failed. NIK already exists\nError: '+error.message); });
    } catch (error) {
      dispatch({
        type: ActionTypes.USER_REGISTER_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      })
    }
  }

export const requestLogin = (creds) => {
    return {
        type: ActionTypes.LOGIN_REQUEST,
        creds
    }
}
  
export const receiveLogin = (response) => {
    return {
        type: ActionTypes.LOGIN_SUCCESS,
        token: response.token
    }
}
  
export const loginError = (message) => {
    return {
        type: ActionTypes.LOGIN_FAILURE,
        message
    }
}

export const loginUser = (creds) => (dispatch) => {
    // We dispatch requestLogin to kickoff the call to the API
    dispatch(requestLogin(creds))

    return fetch(baseUrl + 'users/login', {
        method: 'POST',
        headers: { 
            'Content-Type':'application/json' 
        },
        body: JSON.stringify(creds)
    })
    .then(response => {
        if (response.ok) {
            return response;
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
        },
        error => {
            throw error;
        })
    .then(response => response.json())
    .then(response => {
        if (response.success) {
            // If login was successful, set the token in local storage
            localStorage.setItem('userInfo', JSON.stringify(response.user));
            localStorage.setItem('token', response.token); 
            dispatch(receiveLogin(response))
            // localStorage.setItem('isCheckin', true)
            // localStorage.setItem('isCheckout', true)
           
            dispatch(fetchCheckin()).then(res => localStorage.setItem('isCheckin', JSON.stringify(res.payload.isCheckin)))
            dispatch(fetchCheckout())
            .then(res => {localStorage.setItem('isCheckout', JSON.stringify(res.payload.isCheckout));
            window.location.href = `${process.env.PUBLIC_URL}/dashboard/homepage`                       
            })
            
               
            // localStorage.setItem('creds', JSON.stringify(creds));
            // Dispatch the success action
            // dispatch(fetchFavorites());
                    
        }
        else {
            var error = new Error('Error ' + response.status);
            error.response = response;
            throw error;
        }
    })
    // .then(() => {window.location.href = `${process.env.PUBLIC_URL}/dashboard/homepage`})
    .catch(error => {alert('Wrong NIK or Password'); dispatch(loginError(error.message))})
};

export const requestLogout = () => {
    return {
      type: ActionTypes.LOGOUT_REQUEST
    }
}
  
export const receiveLogout = () => {
    return {
      type: ActionTypes.LOGOUT_SUCCESS
    }
}

// Logs the user out
export const logoutUser = () => (dispatch) => {
    dispatch(requestLogout())
    localStorage.removeItem('token');
    localStorage.removeItem('userInfo');
    dispatch(favoritesFailed("Error 401: Unauthorized"));
    dispatch(receiveLogout())
}

export const updateUser = (userId,data) => (dispatch) => {

    dispatch(requestLogin(data))
    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'users/' + userId, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(response => {
        if (response.success) {
            localStorage.setItem('userInfo', JSON.stringify(response.user));
            dispatch(receiveLogin(response));   
        } 
        else {
            var error = new Error('Error ' + response.status);
            error.response = response;
            throw error;
        }   
    })
    .then(() => {window.location.href = `${process.env.PUBLIC_URL}/dashboard/homepage`})
    .catch(error => dispatch(loginError(error.message)))
}

// PRESENSI

export const userCheckin = (checkin) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'presensi/checkin', {
        method: "POST",
        body: JSON.stringify(checkin),
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(checkin => {
        console.log('You are checked in'); 
        localStorage.setItem('isCheckin', true);
        dispatch(userCheckinSuccess(checkin));        
    })
    .then(() => {window.location.href = `${process.env.PUBLIC_URL}/dashboard/homepage`})
    .catch(error => dispatch(userCheckinFail(error.message)));
}

export const userCheckinSuccess = (checkin) => ({
    type: ActionTypes.USER_CHECKIN_SUCCESS,
    payload: checkin
});

export const userCheckinFail = (errmess) => ({
    type: ActionTypes.USER_CHECKIN_FAIL,
    payload: errmess
});

export const userCheckout = (checkout) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'presensi/checkout', {
        method: "PUT",
        body: JSON.stringify(checkout),
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(checkout => {

        localStorage.setItem('isCheckout', true);
        dispatch(userCheckoutSuccess(checkout)) 
      
    })        
    .then(() => {window.location.href = `${process.env.PUBLIC_URL}/dashboard/homepage`})
    .catch(error => dispatch(userCheckoutFail(error.message)));
}

export const userCheckoutSuccess = (checkout) => ({
    type: ActionTypes.USER_CHECKOUT_SUCCESS,
    payload: checkout
});

export const userCheckoutFail = (errmess) => ({
    type: ActionTypes.USER_CHECKOUT_FAIL,
    payload: errmess
});

export const fetchPresensi = (presensi) => (dispatch) => {
    dispatch(fetchPresensiRequest(presensi))
    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'presensi', {
        headers: {
            'Authorization': bearer
        },
    })
    .then(response => {
        if (response.ok) {
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        var errmess = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(presensis => dispatch(fetchPresensiSuccess(presensis)))
    .catch(error => dispatch(fetchPresensiFail(error.message)));
}

export const fetchCheckin = (presensi) => (dispatch) => {
    dispatch(fetchPresensiRequest(presensi))
    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'presensi/checkin', {
        headers: {
            'Authorization': bearer
        },
    })
    .then(response => {
        if (response.ok) {
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        var errmess = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(isCheckin => dispatch(fetchPresensiSuccess(isCheckin)))
    .catch(error => dispatch(fetchPresensiFail(error.message)));
}

export const fetchCheckout = (presensi) => (dispatch) => {
    dispatch(fetchPresensiRequest(presensi))
    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'presensi/checkout', {
        headers: {
            'Authorization': bearer
        },
    })
    .then(response => {
        if (response.ok) {
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        var errmess = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(isCheckin => dispatch(fetchPresensiSuccess(isCheckin)))
    .catch(error => dispatch(fetchPresensiFail(error.message)));
}

export const fetchPresensiRequest = (presensi) => ({
    type: ActionTypes.PRESENSI_FETCH_REQUEST,
    payload: presensi
});

export const fetchPresensiSuccess = (activity) => ({
    type: ActionTypes.PRESENSI_FETCH_SUCCESS,
    payload: activity
});

export const fetchPresensiFail = (errmess) => ({
    type: ActionTypes.PRESENSI_FETCH_FAIL,
    payload: errmess
});

// ACTIVITY
export const createActivity = (activity) => (dispatch) => {

    dispatch(createActivityRequest(activity))
    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'activities/', {
        method: "POST",
        body: JSON.stringify(activity),
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(activity => {console.log('Activity Created'); dispatch(createActivitySuccess(activity))})
    .catch(error => dispatch(createActivityFail(error.message)));
}

export const fetchActivities = () => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'activities', {
        headers: {
            'Authorization': bearer
        },
    })
    .then(response => {
        if (response.ok) {
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        var errmess = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(activities => dispatch(createActivitySuccess(activities)))
    .catch(error => dispatch(createActivityFail(error.message)));
}

export const fetchActivity = (activityId) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'activities/'+ activityId, {
        headers: {
            'Authorization': bearer
        },
    })
    .then(response => {
        if (response.ok) {
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        var errmess = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(activity => dispatch(createActivitySuccess(activity)))
    .catch(error => dispatch(createActivityFail(error.message)));
}

export const updateActivity = (activityId,activity) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'activities/' + activityId, {
        method: "PUT",
        body: JSON.stringify(activity),
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(activity => dispatch(createActivitySuccess(activity)))
    .catch(error => dispatch(createActivityFail(error.message)));
}

export const deleteActivity = (activityId) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'activities/' + activityId, {
        method: "DELETE",
        headers: {
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(activities => { console.log('Activity Deleted', activities); dispatch(createActivityRequest(activities)); })
    .catch(error => dispatch(createActivityFail(error.message)));
};

export const createActivityRequest = (activity) => ({
    type: ActionTypes.ACTIVITY_CREATE_REQUEST,
    payload: activity
});

export const createActivitySuccess = (activity) => ({
    type: ActionTypes.ACTIVITY_CREATE_SUCCESS,
    payload: activity
});

export const createActivityFail = (errmess) => ({
    type: ActionTypes.ACTIVITY_CREATE_FAIL,
    payload: errmess
});

// BLOG
export const createBlog = (blog) => (dispatch) => {

    dispatch(createBlogRequest(blog))
    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'blog/', {
        method: "POST",
        body: JSON.stringify(blog),
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(blog => {console.log('Blog Created'); dispatch(createBlogSuccess(blog))})
    .catch(error => dispatch(createBlogFail(error.message)));
}

export const fetchBlogs = () => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'blog', {
        headers: {
            'Authorization': bearer
        },
    })
    .then(response => {
        if (response.ok) {
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        var errmess = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(blogs => dispatch(createBlogSuccess(blogs)))
    .catch(error => dispatch(createBlogFail(error.message)));
}

export const fetchBlog = (postId) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'blog/'+ postId, {
        headers: {
            'Authorization': bearer
        },
    })
    .then(response => {
        if (response.ok) {
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        var errmess = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(blog => dispatch(createBlogSuccess(blog)))
    .catch(error => dispatch(createBlogFail(error.message)));
}

export const updateBlog = (blogId,blog) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'blog/' + blogId, {
        method: "PUT",
        body: JSON.stringify(blog),
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(blog => dispatch(createBlogSuccess(blog)))
    .catch(error => dispatch(createBlogFail(error.message)));
}

export const deleteBlog = (blogId) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'blog/' + blogId, {
        method: "DELETE",
        headers: {
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(blogs => { console.log('Blog Deleted', blogs); dispatch(createBlogRequest(blogs)); })
    .catch(error => dispatch(createBlogFail(error.message)));
};

export const createBlogRequest = (blog) => ({
    type: ActionTypes.BLOG_CREATE_REQUEST,
    payload: blog
});

export const createBlogSuccess = (blog) => ({
    type: ActionTypes.BLOG_CREATE_SUCCESS,
    payload: blog
});

export const createBlogFail = (errmess) => ({
    type: ActionTypes.BLOG_CREATE_FAIL,
    payload: errmess
});


export const addComment = (comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: comment
});

export const postComment = (dishId, rating, comment) => (dispatch) => {

    const newComment = {
        dish: dishId,
        rating: rating,
        comment: comment
    }
    console.log('Comment ', newComment);

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'comments', {
        method: 'POST',
        body: JSON.stringify(newComment),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': bearer
        },
        credentials: 'same-origin'
    })
    .then(response => {
        if (response.ok) {
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        var errmess = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(response => dispatch(addComment(response)))
    .catch(error => { console.log('Post comments ', error.message);
        alert('Your comment could not be posted\nError: '+ error.message); })
}

export const fetchDishes = () => (dispatch) => {
    dispatch(dishesLoading(true));

    return fetch(baseUrl + 'dishes')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
        .then(response => response.json())
        .then(dishes => dispatch(addDishes(dishes)))
        .catch(error => dispatch(dishesFailed(error.message)));
}

export const dishesLoading = () => ({
    type: ActionTypes.DISHES_LOADING
});

export const dishesFailed = (errmess) => ({
    type: ActionTypes.DISHES_FAILED,
    payload: errmess
});

export const addDishes = (dishes) => ({
    type: ActionTypes.ADD_DISHES,
    payload: dishes
});

export const fetchComments = () => (dispatch) => {
    return fetch(baseUrl + 'comments')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
        .then(response => response.json())
        .then(comments => dispatch(addComments(comments)))
        .catch(error => dispatch(commentsFailed(error.message)));
}

export const commentsFailed = (errmess) => ({
    type: ActionTypes.COMMENTS_FAILED,
    payload: errmess
});

export const addComments = (comments) => ({
    type: ActionTypes.ADD_COMMENTS,
    payload: comments
});

export const fetchPromos = () => (dispatch) => {
    dispatch(promosLoading(true));

    return fetch(baseUrl + 'promotions')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
        .then(response => response.json())
        .then(promos => dispatch(addPromos(promos)))
        .catch(error => dispatch(promosFailed(error.message)));
}

export const promosLoading = () => ({
    type: ActionTypes.PROMOS_LOADING
});

export const promosFailed = (errmess) => ({
    type: ActionTypes.PROMOS_FAILED,
    payload: errmess
});

export const addPromos = (promos) => ({
    type: ActionTypes.ADD_PROMOS,
    payload: promos
});

export const fetchLeaders = () => (dispatch) => {
    
    dispatch(leadersLoading());

    return fetch(baseUrl + 'leaders')
    .then(response => {
        if (response.ok) {
            return response;
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
    .then(response => response.json())
    .then(leaders => dispatch(addLeaders(leaders)))
    .catch(error => dispatch(leadersFailed(error.message)));
}

export const leadersLoading = () => ({
    type: ActionTypes.LEADERS_LOADING
});

export const leadersFailed = (errmess) => ({
    type: ActionTypes.LEADERS_FAILED,
    payload: errmess
});

export const addLeaders = (leaders) => ({
    type: ActionTypes.ADD_LEADERS,
    payload: leaders
});

export const postFeedback = (feedback) => (dispatch) => {
        
    return fetch(baseUrl + 'feedback', {
        method: "POST",
        body: JSON.stringify(feedback),
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(response => { console.log('Feedback', response); alert('Thank you for your feedback!\n'+JSON.stringify(response)); })
    .catch(error =>  { console.log('Feedback', error.message); alert('Your feedback could not be posted\nError: '+error.message); });
};

// FAVORITE

export const postFavorite = (dishId) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'favorites/' + dishId, {
        method: "POST",
        body: JSON.stringify({"_id": dishId}),
        headers: {
          "Content-Type": "application/json",
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(favorites => { console.log('Favorite Added', favorites); dispatch(addFavorites(favorites)); })
    .catch(error => dispatch(favoritesFailed(error.message)));
}

export const deleteFavorite = (dishId) => (dispatch) => {

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'favorites/' + dishId, {
        method: "DELETE",
        headers: {
          'Authorization': bearer
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(favorites => { console.log('Favorite Deleted', favorites); dispatch(addFavorites(favorites)); })
    .catch(error => dispatch(favoritesFailed(error.message)));
};

export const fetchFavorites = () => (dispatch) => {
    dispatch(favoritesLoading(true));

    const bearer = 'Bearer ' + localStorage.getItem('token');

    return fetch(baseUrl + 'favorites', {
        headers: {
            'Authorization': bearer
        },
    })
    .then(response => {
        if (response.ok) {
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        var errmess = new Error(error.message);
        throw errmess;
    })
    .then(response => response.json())
    .then(favorites => dispatch(addFavorites(favorites)))
    .catch(error => dispatch(favoritesFailed(error.message)));
}

export const favoritesLoading = () => ({
    type: ActionTypes.FAVORITES_LOADING
});

export const favoritesFailed = (errmess) => ({
    type: ActionTypes.FAVORITES_FAILED,
    payload: errmess
});

export const addFavorites = (favorites) => ({
    type: ActionTypes.ADD_FAVORITES,
    payload: favorites
});
