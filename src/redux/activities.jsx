import * as ActionTypes from './actionTypes';

export const Activities = (state = {}, action) => {
    switch (action.type) {
      case ActionTypes.ACTIVITY_CREATE_REQUEST:
        return { isLoading: true }
      case ActionTypes.ACTIVITY_CREATE_SUCCESS:
        return { isLoading: false, activity: action.payload }
      case ActionTypes.ACTIVITY_CREATE_FAIL:
        return { isLoading: false, error: action.payload }
      case ActionTypes.ACTIVITY_UPDATE_REQUEST:
        return { isLoading: true }
      case ActionTypes.ACTIVITY_UPDATE_SUCCESS:
        return { isLoading: false, activity: action.payload }
      case ActionTypes.ACTIVITY_UPDATE_FAIL:
        return { isLoading: false, error: action.payload }
      case ActionTypes.ACTIVITY_DELETE_REQUEST:
        return { loading: true }
      case ActionTypes.ACTIVITY_DELETE_SUCCESS:
        return { isLoading: false, success: true }
      case ActionTypes.ACTIVITY_DELETE_FAIL:
        return { isLoading: false, error: action.payload }
      default:
        return state
    }
  }